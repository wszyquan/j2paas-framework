/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.event;

import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.task.zkex.ListSupport;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Treeitem;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RowEntry {

    private ListSupport support;

    private Component item;

    private EventDispatcher dispatcher;

    public RowEntry(EventDispatcher dispatcher, ListSupport support,
                    Component item) {
        this.dispatcher = dispatcher;
        this.support = support;
        this.item = item;
    }

    public Object getValue(int index) {
        ListRowVo rv = null;
        if (item instanceof Listitem)
            rv = ((Listitem) item).getValue();
        else
            rv = ((Treeitem) item).getValue();
        return rv.getData()[index];
    }

    public Object getValue(String name) {
        return dispatcher.getItemData(item, name);
    }

    public void setValue(int index, Object value) {
        ListRowVo rv = null;
        if (item instanceof Listitem)
            rv = ((Listitem) item).getValue();
        else
            rv = ((Treeitem) item).getValue();
        rv.getData()[index] = value;
    }

    public void setValue(String name, Object value) {
        dispatcher.setData(support, item, name, value);
    }

    public Component getItem() {
        return item;
    }
}
