/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.event;

import cn.easyplatform.lang.Nums;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.impl.FormatInputElement;
import org.zkoss.zul.impl.InputElement;

import java.text.DecimalFormat;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AccValueChangeEventListener implements EventListener<Event> {

    private String id;

    private Component target;

    public AccValueChangeEventListener(String id, Component target) {
        this.id = id;
        this.target = target;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        InputElement comp = (InputElement) event.getTarget();
        Object value = PageUtils.getValue(comp, false);
        if (value != null) {
            String format = WebUtils.getFormat(id, (String) value);
            if (target instanceof FormatInputElement) {
                FormatInputElement input = (FormatInputElement) target;
                input.setFormat(format);
            } else {
                value = PageUtils.getValue(target, false);
                if (!(value instanceof Number))
                    value = Nums.toDouble(value, 0);
                DecimalFormat df = new DecimalFormat(format);
                PageUtils.setValue(target, df.format(value));
            }
        }
    }
}
