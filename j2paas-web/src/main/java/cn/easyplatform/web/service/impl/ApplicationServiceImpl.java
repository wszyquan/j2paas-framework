/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service.impl;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.service.ApplicationService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ApplicationServiceImpl implements ApplicationService {

    private ApplicationService broker = EngineFactory.me().getEngineService(
            ApplicationService.class);

    @Override
    public IResponseMessage<?> init(SimpleRequestMessage req) {
        return broker.init(req);
    }

    @Override
    public IResponseMessage<?> ttl(SimpleRequestMessage req) {
        return broker.ttl(req);
    }

    @Override
    public IResponseMessage<?> getRuntime(SimpleRequestMessage req) {
        return broker.getRuntime(req);
    }

    @Override
    public IResponseMessage<?> purge(SimpleRequestMessage req) {
        return broker.purge(req);
    }

    @Override
    public IResponseMessage<?> debug(DebugRequestMessage req) {
        return broker.debug(req);
    }

    @Override
    public IResponseMessage<?> executeExpression(ExpressionRequestMessage req) {
        return broker.executeExpression(req);
    }

    @Override
    public IResponseMessage<?> switchOrg(SwitchOrgRequestMessage req) {
        return broker.switchOrg(req);
    }

    @Override
    public IResponseMessage<?> i18n(SimpleTextRequestMessage req) {
        return broker.i18n(req);
    }

    @Override
    public IResponseMessage<?> getAcc(GetAccRequestMessage req) {
        return broker.getAcc(req);
    }

    @Override
    public IResponseMessage<?> getWizard(SimpleTextRequestMessage req) {
        return broker.getWizard(req);
    }

    @Override
    public IResponseMessage<?> getConfig(SimpleTextRequestMessage req) {
        return broker.getConfig(req);
    }
}
