/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.type.IResponseMessage;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractMessageDlg implements MessageDlg {

    protected EventListener<Event> sourceListener;

    protected String id;

    protected String code;

    protected String message;

    /**
     * @param resp
     * @param sourceListener
     */
    public AbstractMessageDlg(IResponseMessage<?> resp,
                              EventListener<Event> sourceListener) {
        this.id = resp.getId();
        this.code = resp.getCode();
        this.message = (String) resp.getBody();
        this.sourceListener = sourceListener;
    }
}
