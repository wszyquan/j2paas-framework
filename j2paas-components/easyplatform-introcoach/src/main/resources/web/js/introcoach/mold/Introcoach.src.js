function (out) {
    var uuid = this.uuid,
        icon = this.$s('icon');
    out.push('<div', this.domAttrs_(), '>');
    out.push('<div id="', uuid, '-pointer" class="', this.$s('pointer'), '"></div>');
    out.push('<div id="', uuid, '-cave" class="', this.$s('content'), '">');
    for (var w = this.firstChild; w; w = w.nextSibling)
        w.redraw(out);
    out.push('</div>');
    /*out.push('<div id="', uuid, '-cls" class="', this.$s('close'),
        '"><i id="', uuid, '-clsIcon" class="', icon, ' z-icon-times"></i></div>');*/
    out.push('</div>');
}