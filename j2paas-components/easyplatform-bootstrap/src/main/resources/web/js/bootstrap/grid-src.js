zk.afterLoad('zul.grid', function () {
    let _grid = {}, _columns = {}, _column = {}, _rows = {}, _row = {}, _molds = {};
    zk.override(zul.grid.Grid.molds, _molds, {
        'bs': zul.grid.Grid.molds['default']
    });
    zk.override(zul.grid.Grid.prototype, _grid, {
        _inBSMold: function () {
            return this._mold == 'bs';
        },
        bind_: function (desktop, skipper, after) {
            if (this._inBSMold()) {
                this.$supers(zul.grid.Grid, 'bind_', arguments);
            } else
                _grid.bind_.apply(this, arguments);
        },
        redraw: function (out) {
            if (this._inBSMold()) {
                let uuid = this.uuid,
                    innerWidth = this.getInnerWidth(),
                    wdAttr = innerWidth == '100%' ? ' width="100%"' : '', //bug#3183182
                    wdStyle = innerWidth != '100%' ? 'width:' + innerWidth : '',
                    inPaging = this.inPagingMold(), pgpos;
                out.push('<div', this.domAttrs_(), '>');
                if (inPaging && this.paging) {
                    pgpos = this.getPagingPosition();
                    if (pgpos == 'top' || pgpos == 'both') {
                        out.push('<div id="', uuid, '-pgit" class="', this.$s('paging-top'), '">');
                        this.paging.redraw(out);
                        out.push('</div>');
                    }
                }
                out.push('<div id="', uuid, '-body" class="', this.$s('body'));
                if (this._autopaging)
                    out.push(' ', this.$s('autopaging'));
                out.push('"');
                var hgh = this.getHeight(),
                    iOSNativeBar = zk.ios && this._nativebar;
                if (hgh || iOSNativeBar)
                    out.push(' style="', hgh ? 'height:' + hgh + ';' : '', iOSNativeBar ? '-webkit-overflow-scrolling:touch;' : '', '"');
                out.push('>');
                if (this.rows && this.domPad_ && !this.inPagingMold())
                    this.domPad_(out, '-tpad');

                out.push('<table id="', uuid, '-cave"', wdAttr, ' style="table-layout:fixed;', wdStyle, '">');
                if (this.columns) {
                    out.push('<thead id="', this.uuid, '-headrows">');
                    for (let hds = this.heads, j = 0, len = hds.length; j < len;)
                        hds[j++].redraw(out);
                    out.push('</thead>');
                }
                if (this.rows)
                    this.rows.redraw(out);
                //this.redrawEmpty_(out);
                out.push('</table>');
                if (this.rows && this.domPad_ && !this.inPagingMold())
                    this.domPad_(out, '-bpad');
                out.push('</div>');

                if (pgpos == 'bottom' || pgpos == 'both') {
                    out.push('<div id="', uuid, '-pgib" class="', this.$s('paging-bottom'), '">');
                    this.paging.redraw(out);
                    out.push('</div>');
                }
                out.push('</div>');
            } else
                _grid.redraw.apply(this, arguments);
        },
        getZclass: function () {
            if (this._inBSMold()) {
                return this._zclass ? this._zclass : 'table';
            } else
                return _grid.getZclass.apply(this, arguments);
        },
        _syncEmpty: function () {
            this._shallFixEmpty = !this._inBSMold();
        }
    });
    zk.override(zul.grid.Columns.prototype, _columns, {
        _inBSMold: function () {
            return this.parent && this.parent._inBSMold && this.parent._inBSMold();
        },
        redraw: function (out) {
            if (this._inBSMold()) {
                out.push('<tr', this.domAttrs_(), ' style="text-align: left;">');
                for (var w = this.firstChild; w; w = w.nextSibling)
                    w.redraw(out);
                out.push('</tr>');
            } else
                _columns.redraw.apply(this, arguments);
        },
        getZclass: function () {
            if (this._inBSMold()) {
                return this._zclass ? this._zclass : '';
            } else
                return _columns.getZclass.apply(this, arguments);
        }
    });
    zk.override(zul.grid.Column.prototype, _column, {
        _inBSMold: function () {
            return this.parent && this.parent._inBSMold && this.parent._inBSMold();
        },
        redraw: function (out) {
            if (this._inBSMold()) {
                out.push('<th scope="col" ', this.domAttrs_(), '>');
                let label = this.domContent_();
                out.push(label === '' ? '&nbsp;' : label);
                out.push('</th>');
            } else
                _column.redraw.apply(this, arguments);
        },
        getZclass: function () {
            if (this._inBSMold()) {
                return this._zclass ? this._zclass : '';
            } else
                return _column.getZclass.apply(this, arguments);
        }
    });
    zk.override(zul.grid.Rows.prototype, _rows, {
        _inBSMold: function () {
            return this.parent && this.parent._inBSMold && this.parent._inBSMold();
        },
        getZclass: function () {
            if (this._inBSMold()) {
                return this._zclass ? this._zclass : '';
            } else
                return _rows.getZclass.apply(this, arguments);
        }
    });
    zk.override(zul.grid.Row.prototype, _row, {
        _inBSMold: function () {
            return this.parent && this.parent._inBSMold && this.parent._inBSMold();
        },
        encloseChildHTML_: function (opts) {
            if (this._inBSMold()) {
                let out = opts.out || new zk.Buffer(),
                    child = opts.child,
                    isCell = child.$instanceof(zul.wgt.Cell);

                if (isCell)
                    child._headerVisible = opts.visible;
                else {
                    out.push('<td id="', child.uuid, '-chdextr"',
                        this._childAttrs(child, opts.index), '>');
                }
                child.redraw(out);
                if (!isCell)
                    out.push('</td>');
                if (!opts.out)
                    return out.join('');
            } else
                return _row.encloseChildHTML_.apply(this, arguments);
        },
        _childAttrs: function (child, index) {
            let attrs = _row._childAttrs.apply(this, arguments);
            if (this._inBSMold()) {
                return attrs.substring(0, attrs.indexOf(" class="));
            } else
                return attrs;
        },
        getSclass: function () {
            if (this._inBSMold()) {
                return '';
            } else
                return _row.getSclass.apply(this, arguments);
        },
        $s: function (subclass) {
            if (this._inBSMold())
                return '';
            return _row.$s.apply(this, arguments);
        },
        getZclass: function () {
            if (this._inBSMold()) {
                return this._zclass ? this._zclass : '';
            } else
                return _row.getZclass.apply(this, arguments);
        }
    });
});

zk.load('zul.mesh', function () {
    let _mesh = {};
    zk.override(zul.mesh.MeshWidget.prototype, _mesh, {
        getPageSize: function () {
            let pg = (this.paging || this._paginal);
            if (pg)
                return pg.getPageSize();
            return 0;
        },
        getPageCount: function () {
            let pg = (this.paging || this._paginal);
            if (pg)
                return pg.getPageCount();
            return 0;
        },
        getActivePage: function () {
            let pg = (this.paging || this._paginal);
            if (pg)
                return pg.getActivePage();
            return 0;
        }
    });
});