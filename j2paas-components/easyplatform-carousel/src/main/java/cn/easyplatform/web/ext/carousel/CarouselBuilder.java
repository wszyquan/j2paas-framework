/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.carousel;

import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.Widget;
import com.alibaba.fastjson.JSON;
import org.zkoss.zk.ui.Component;

import java.util.ArrayList;
import java.util.List;

public class CarouselBuilder implements ComponentBuilder, Widget {

    private ComponentHandler dataHandler;

    private Carousel carousel;

    public CarouselBuilder(ComponentHandler dataHandler, Carousel carousel) {
        this.dataHandler = dataHandler;
        this.carousel = carousel;
    }


    public Component build() {
        carousel.setAttribute("$proxy", this);
        create(carousel);
        return this.carousel;
    }


    private void create(Carousel cbx) {

        if (cbx.getSequence() != null && cbx.getSequence().length != 0) {

            int imgNum = getIndex(cbx.getSequence(), "img");
            int titleNum = getIndex(cbx.getSequence(), "title");
            int contentNum = getIndex(cbx.getSequence(), "content");
            int imgevtNum = getIndex(cbx.getSequence(), "imgevt");
            int imgurlNum = getIndex(cbx.getSequence(), "imgurl");


            if (null != cbx.getQuery() && !"".equals(cbx.getQuery())) {
                List<?> data = dataHandler.selectList0(Object[].class, cbx.getDbid(), (String) cbx.getQuery());

                List<String> imgStr = new ArrayList<String>();
                List<String> titleStr = new ArrayList<String>();
                List<String> contentStr = new ArrayList<String>();
                List<String> imgevtStr = new ArrayList<String>();
                List<String> imgurlStr = new ArrayList<String>();

                if (data != null && !data.isEmpty()) {
                    for (int i = 0; i < data.size(); i++) {
                        Object[] obj = (Object[]) data.get(i);

                        if (imgNum != -1 && obj.length > imgNum) {
                            imgStr.add((String) obj[imgNum]);
                        }
                        if (titleNum != -1 && obj.length > titleNum) {
                            titleStr.add((String) obj[titleNum]);
                        }
                        if (contentNum != -1 && obj.length > contentNum) {
                            contentStr.add((String) obj[contentNum]);
                        }
                        if (imgevtNum != -1 && obj.length > imgevtNum) {
                            imgevtStr.add((String) obj[imgevtNum]);
                        }
                        if (imgurlNum != -1 && obj.length > imgurlNum) {
                            imgurlStr.add((String) obj[imgurlNum]);
                        }
                    }

                    if (imgStr.size() > 0) {
                        cbx.setImg(JSON.toJSONString(imgStr));
                    }
                    if (titleStr.size() > 0) {
                        cbx.setTitle(JSON.toJSONString(titleStr));
                    }
                    if (contentStr.size() > 0) {
                        cbx.setContent(JSON.toJSONString(contentStr));
                    }
                    if (imgevtStr.size() > 0) {
                        cbx.setImgevt(JSON.toJSONString(imgevtStr));
                    }
                    if (imgurlStr.size() > 0) {
                        cbx.setImgurl(JSON.toJSONString(imgurlStr));
                    }

                }
            }

            if(cbx.getEvent()!=null && cbx.getEvent()!="") {
                dataHandler.addEventListener("onLink", cbx);
            }

        }
    }


    public void reload(Component widget) {
        Carousel cbx = (Carousel)widget;
        this.create(cbx);
        cbx.setReloadFlag(true);
    }

    public int getIndex(String[] array,String value){
        for(int i = 0;i<array.length;i++){
            if(array[i].equals(value)){
                return i;
            }
        }
        return -1;
    }


}
