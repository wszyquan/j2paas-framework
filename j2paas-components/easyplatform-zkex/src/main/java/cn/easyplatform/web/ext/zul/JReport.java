/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.EntityExt;
import cn.easyplatform.web.ext.ManagedExt;
import org.zkoss.zul.Iframe;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JReport extends Iframe implements EntityExt, ManagedExt {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 报表参数的id
     */
    private String _entity;

    /**
     * 在显示时是否要马上执行报表
     */
    private boolean _immediate = true;

    /**
     * 报表在页面显示的类型:pdf,html
     */
    private String _type = "html";

    /**
     * 数据来源
     */
    private String _from;

    /**
     * 报表状态: C:新建报表 U:先查找旧的报表，如果旧的不存在，新建报表 R:查找旧报表，旧报表不存在则出错
     */
    private char _state = 'C';

    /**
     * 组成id的栏位集合，以逗号分隔 如果ids不为空，表示可从历史记录中加载旧数据或保存新数据
     */
    private String _ids;

    /**
     * 如果数据来源于数据列表，可以指定要排序的顺序,如果有多个栏位，用逗号分隔
     */
    private String _listOrder;

    /**
     * 初始脚本
     */
    private String _init;

    /**
     * 是否要显示进度窗口
     */
    private Boolean _showProgress;

    /**
     * type为pdf时缩放比
     */
    private double _zoom = 1;

    public double getZoom() {
        return _zoom;
    }

    public void setZoom(double zoom) {
        this._zoom = zoom;
    }

    /**
     * @return the showProgress
     */
    public Boolean isShowProgress() {
        return _showProgress;
    }

    /**
     * @param showProgress the showProgress to set
     */
    public void setShowProgress(Boolean showProgress) {
        this._showProgress = showProgress;
    }

    /**
     * @return the init
     */
    public String getInit() {
        return _init;
    }

    /**
     * @param init the init to set
     */
    public void setInit(String init) {
        this._init = init;
    }

    /**
     * @return the listOrder
     */
    public String getListOrder() {
        return _listOrder;
    }

    /**
     * @param listOrder the listOrder to set
     */
    public void setListOrder(String listOrder) {
        this._listOrder = listOrder;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return _from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this._from = from;
    }

    @Override
    public String getEntity() {
        return _entity;
    }

    public void setEntity(String entityId) {
        this._entity = entityId;
    }

    public boolean isImmediate() {
        return _immediate;
    }

    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        this._type = type;
    }

    /**
     * @return the state
     */
    public char getState() {
        return _state;
    }

    /**
     * @param state the state to set
     */
    public void setState(char state) {
        this._state = state;
    }

    /**
     * @return the ids
     */
    public String getIds() {
        return _ids;
    }

    /**
     * @param ids the ids to set
     */
    public void setIds(String ids) {
        this._ids = ids;
    }

}
