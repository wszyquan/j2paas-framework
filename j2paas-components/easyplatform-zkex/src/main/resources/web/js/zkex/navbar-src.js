zk.afterLoad('zkmax.nav', function () {
    var _navbar = {};
    zk.override(zkmax.nav.Navbar.prototype, _navbar, {
        bind_: function () {
            _navbar.bind_.apply(this, arguments);
            if (this.isCollapsed())
                this.setCollapsed(true);
            jq(this.$n('cave')).width('100%').height("100%");
        },
        setCollapsed: function (collapsed) {
            _navbar.setCollapsed.apply(this, arguments);
            var c = this.firstChild;
            while (c && c.widgetName == 'nav') {
                if (collapsed)
                    jq(c.$n('caret')).css('display', 'none');
                else
                    jq(c.$n('caret')).css('display', '');
                c = c.nextSibling;
            }
        },
    });
    var _nav = {};
    zk.override(zkmax.nav.Nav.prototype, _nav, {
        domContent_: function () {
            var c = '<span id="' + this.uuid + '-text" class="'
                + this.$s("text") + '">'
                + (zUtl.encodeXML(this.getLabel())) + "</span>";
            var d = this._badgeText ? '<span id="'
                + this.uuid
                + '-info" class="'
                + this.$s("info")
                + '">'
                + (zUtl.encodeXML(this._badgeText)) + "</span>"
                : "";
            var e = this._badgeText ? '' : '<i id="' + this.uuid + '-caret" class="' + (this._open ? 'z-icon-caret-down' : 'z-icon-caret-left') + ' z-nav-caret"/>';
            var a = this.getImage(), b = this.domIcon_();
            if (a) {
                a = '<img src="' + a + '" class="'
                    + this.$s("image")
                    + '" align="absmiddle" />'
                    + (b ? " " + b : "")
            } else {
                if (b) {
                    a = b
                } else {
                    a = '<img src="data:image/png;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="'
                        + this.$s("image")
                        + '" align="absmiddle" />'
                }
            }
            return a + c + d + e;
        },
        open: function () {
            _nav.open.apply(this, arguments);
            var s = $(this).attr('level');
            if (s)
                $(this).children().css('padding-left', s + 'px');
            if (!this._badgeText)
                jq(this.$n('caret')).attr('class', "z-icon-caret-down z-nav-caret")
        },
        close: function (d) {
            _nav.close.apply(this, arguments);
            if (!this._badgeText)
                jq(this.$n('caret')).attr('class', "z-icon-caret-left z-nav-caret")
        }
    });
});