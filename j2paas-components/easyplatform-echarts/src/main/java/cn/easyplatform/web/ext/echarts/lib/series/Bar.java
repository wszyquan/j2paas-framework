/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Bar extends Series {
    private Boolean legendHoverLink;
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Object stack;
    private Object barWidth;
    private Object barMaxWidth;
    private Object barMinHeight;
    private Object barGap;
    private Object barCategoryGap;

    /**
     * 是否在环形柱条两侧使用圆弧效果。
     */
    private Boolean roundCap;
    /**
     * 是否开启大数据量优化，在数据图形特别多而出现卡顿时候可以开启。
     */
    private Boolean large;
    /**
     * 开启绘制优化的阈值。
     */
    private Object largeThreshold;
    /**
     * 渐进式渲染时每一帧绘制图形数量，设为 0 时不启用渐进式渲染，支持每个系列单独配置
     */
    private Object progressive;
    /**
     * 启用渐进式渲染的图形数量阈值，在单个系列的图形数量超过该阈值时启用渐进式渲染。
     */
    private Object progressiveThreshold;
    /**
     * 分片的方式。可选值：
     * <p>
     * 'sequential': 按照数据的顺序分片。缺点是渲染过程不自然。
     * 'mod': 取模分片，即每个片段中的点会遍布于整个数据，从而能够视觉上均匀得渲染。
     */
    private String progressiveChunkMode;

    public Bar() {
        type = "bar";
    }

    public String progressiveChunkMode() {
        return progressiveChunkMode;
    }

    public Bar progressiveChunkMode(String progressiveChunkMode) {
        this.progressiveChunkMode = progressiveChunkMode;
        return this;
    }

    public Object progressiveThreshold() {
        return progressiveThreshold;
    }

    public Bar progressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
        return this;
    }

    public Object progressive() {
        return progressive;
    }

    public Bar progressive(Object progressive) {
        this.progressive = progressive;
        return this;
    }

    public Object largeThreshold() {
        return largeThreshold;
    }

    public Bar largeThreshold(Object largeThreshold) {
        this.largeThreshold = largeThreshold;
        return this;
    }

    public Boolean large() {
        return large;
    }

    public Bar large(Boolean large) {
        this.large = large;
        return this;
    }

    public Boolean roundCap() {
        return roundCap;
    }

    public Bar roundCap(Boolean roundCap) {
        this.roundCap = roundCap;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public Bar legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Bar coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public Bar xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public Bar yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Object stack() {
        return stack;
    }

    public Bar stack(Object stack) {
        this.stack = stack;
        return this;
    }

    public Object barWidth() {
        return barWidth;
    }

    public Bar barWidth(Object barWidth) {
        this.barWidth = barWidth;
        return this;
    }

    public Object barMaxWidth() {
        return barMaxWidth;
    }

    public Bar barMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
        return this;
    }

    public Object barMinHeight() {
        return barMinHeight;
    }

    public Bar barMinHeight(Object barMinHeight) {
        this.barMinHeight = barMinHeight;
        return this;
    }

    public Object barGap() {
        return barGap;
    }

    public Bar barGap(Object barGap) {
        this.barGap = barGap;
        return this;
    }

    public Object barCategoryGap() {
        return barCategoryGap;
    }

    public Bar barCategoryGap(Object barCategoryGap) {
        this.barCategoryGap = barCategoryGap;
        return this;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Object getStack() {
        return stack;
    }

    public void setStack(Object stack) {
        this.stack = stack;
    }

    public Object getBarWidth() {
        return barWidth;
    }

    public void setBarWidth(Object barWidth) {
        this.barWidth = barWidth;
    }

    public Object getBarMaxWidth() {
        return barMaxWidth;
    }

    public void setBarMaxWidth(Object barMaxWidth) {
        this.barMaxWidth = barMaxWidth;
    }

    public Object getBarMinHeight() {
        return barMinHeight;
    }

    public void setBarMinHeight(Object barMinHeight) {
        this.barMinHeight = barMinHeight;
    }

    public Object getBarGap() {
        return barGap;
    }

    public void setBarGap(Object barGap) {
        this.barGap = barGap;
    }

    public Object getBarCategoryGap() {
        return barCategoryGap;
    }

    public void setBarCategoryGap(Object barCategoryGap) {
        this.barCategoryGap = barCategoryGap;
    }

    public Boolean getRoundCap() {
        return roundCap;
    }

    public void setRoundCap(Boolean roundCap) {
        this.roundCap = roundCap;
    }

    public Boolean getLarge() {
        return large;
    }

    public void setLarge(Boolean large) {
        this.large = large;
    }

    public Object getLargeThreshold() {
        return largeThreshold;
    }

    public void setLargeThreshold(Object largeThreshold) {
        this.largeThreshold = largeThreshold;
    }

    public Object getProgressive() {
        return progressive;
    }

    public void setProgressive(Object progressive) {
        this.progressive = progressive;
    }

    public Object getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }

    public String getProgressiveChunkMode() {
        return progressiveChunkMode;
    }

    public void setProgressiveChunkMode(String progressiveChunkMode) {
        this.progressiveChunkMode = progressiveChunkMode;
    }
}
