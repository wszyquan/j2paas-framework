/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class SSAO implements Serializable {
    /**
     * 是否开启环境光遮蔽。默认不开启。
     */
    private boolean enable;
    /**
     * 环境光遮蔽的质量。支持'low', 'medium', 'high', 'ultra'。
     */
    private String quality;
    /**
     * 环境光遮蔽的采样半径。半径越大效果越自然，但是需要设置较高的'quality'。
     */
    private int radius;
    /**
     * 环境光遮蔽的强度。值越大颜色越深。
     */
    private int intensity;

    public Boolean enable() {
        return this.enable;
    }
    public SSAO enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public String quality() {
        return this.quality;
    }
    public SSAO quality(String quality) {
        this.quality = quality;
        return this;
    }

    public Integer radius() {
        return this.radius;
    }
    public SSAO radius(Integer radius) {
        this.radius = radius;
        return this;
    }

    public Integer intensity() {
        return this.intensity;
    }
    public SSAO intensity(Integer intensity) {
        this.intensity = intensity;
        return this;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }
}
