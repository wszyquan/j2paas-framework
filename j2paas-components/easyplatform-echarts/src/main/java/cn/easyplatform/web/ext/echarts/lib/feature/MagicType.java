/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.feature;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import org.zkoss.util.resource.Labels;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MagicType extends Feature {

    private Object type;

    private Option option;

    private Option seriesIndex;

    private Icon icon;

    private ItemStyle iconStyle;

    public MagicType() {
        this.show(true);
        Map title = new HashMap<String, String>();
        title.put("line", Labels.getLabel("echarts.title.line"));
        title.put("bar", Labels.getLabel("echarts.title.bar"));
        title.put("stack", Labels.getLabel("echarts.title.stack"));
        title.put("tiled", Labels.getLabel("echarts.title.tiled"));
        this.title(title);
    }

    public MagicType iconStyle(ItemStyle iconStyle) {
        this.iconStyle = iconStyle;
        return this;
    }

    public ItemStyle iconStyle() {
        if (this.iconStyle == null)
            this.iconStyle = new ItemStyle();
        return this.iconStyle;
    }

    public Object icon() {
        if (icon == null)
            icon = new Icon();
        return this.icon;
    }

    public MagicType icon(Icon Icon) {
        this.icon = Icon;
        return this;
    }

    public Object type() {
        return this.type;
    }

    public MagicType type(Object value) {
        if (value instanceof String)
            this.type = ((String) value).split(",");
        else
            this.type = value;
        return this;
    }

    public MagicType option(Option option) {
        this.option = option;
        return this;
    }

    public Option option() {
        if (this.option == null)
            this.option = new Option();
        return this.option;
    }

    public Option seriesIndex() {
        if (seriesIndex == null)
            seriesIndex = new Option();
        return this.seriesIndex;
    }

    public MagicType seriesIndex(Option seriesIndex) {
        this.option = seriesIndex;
        return this;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public Option getSeriesIndex() {
        return seriesIndex;
    }

    public void setSeriesIndex(Option seriesIndex) {
        this.seriesIndex = seriesIndex;
    }

    public static class Icon {
        private String line;
        private String bar;
        private String stack;
        private String tiled;

        public String stack() {
            return stack;
        }

        public void line(String line) {
            this.line = line;
        }

        public String line() {
            return line;
        }

        public void bar(String bar) {
            this.bar = bar;
        }

        public String bar() {
            return bar;
        }

        public void stack(String stack) {
            this.stack = stack;
        }

        public String tiled() {
            return tiled;
        }

        public void tiled(String tiled) {
            this.tiled = tiled;
        }

        public String getLine() {
            return line;
        }

        public void setLine(String line) {
            this.line = line;
        }

        public String getBar() {
            return bar;
        }

        public void setBar(String bar) {
            this.bar = bar;
        }

        public String getStack() {
            return stack;
        }

        public void setStack(String stack) {
            this.stack = stack;
        }

        public String getTiled() {
            return tiled;
        }

        public void setTiled(String tiled) {
            this.tiled = tiled;
        }
    }

    public static class Option {
        private Object line;
        private Object bar;
        private Object stack;
        private Object tiled;

        public Option line(Object value) {
            if (value instanceof String)
                this.line = ((String) value).split(",");
            else
                this.line = value;
            return this;
        }

        public Object line() {
            return this.line;
        }

        public Option bar(Object value) {
            if (value instanceof String)
                this.bar = ((String) value).split(",");
            else
                this.bar = value;
            return this;
        }

        public Object bar() {
            return this.bar;
        }

        public Option stack(Object value) {
            if (value instanceof String)
                this.stack = ((String) value).split(",");
            else
                this.stack = value;
            return this;
        }

        public Object stack() {
            return this.stack;
        }

        public Option tiled(Object value) {
            if (value instanceof String)
                this.tiled = ((String) value).split(",");
            else
                this.tiled = value;
            return this;
        }

        public Object tiled() {
            return this.tiled;
        }

        public Object getLine() {
            return line;
        }

        public void setLine(Object line) {
            this.line = line;
        }

        public Object getBar() {
            return bar;
        }

        public void setBar(Object bar) {
            this.bar = bar;
        }

        public Object getStack() {
            return stack;
        }

        public void setStack(Object stack) {
            this.stack = stack;
        }

        public Object getTiled() {
            return tiled;
        }

        public void setTiled(Object tiled) {
            this.tiled = tiled;
        }
    }
}
