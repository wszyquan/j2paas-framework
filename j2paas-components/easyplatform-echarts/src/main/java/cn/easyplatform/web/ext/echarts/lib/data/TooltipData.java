/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import java.io.Serializable;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TooltipData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    /**
     * 事件或主题的名称。
     */
    private String name;
    /**
     * 时间或主题的时间属性
     */
    private String date;
    /**
     * 事件或主题在某个时间点的值
     */
    private Object value;

    public TooltipData(String name) {
        this.name = name;
    }

    public String name() {
        return this.name;
    }

    public TooltipData name(String name) {
        this.name = name;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public TooltipData value(Object value) {
        this.value = value;
        return this;
    }

    public String date() {
        return this.date;
    }

    public TooltipData date(String date) {
        this.date = date;
        return this;
    }

}
