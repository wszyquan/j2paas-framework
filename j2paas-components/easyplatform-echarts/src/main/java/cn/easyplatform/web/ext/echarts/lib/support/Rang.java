/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.support;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Rang {
    private Object symbol;// 图元的图形类别。
    private Object symbolSize;// 图元的大小。
    private Object color;// 图元的颜色。
    private Object colorAlpha;//图元的颜色的透明度。
    private Object opacity;// 图元以及其附属物（如文字标签）的透明度。
    private Object colorLightness;//颜色的明暗度，参见 HSL。
    private Object colorSaturation;// 颜色的饱和度，参见 HSL。
    private Object colorHue;//颜色的色调，参见 HSL。

    public Object symbol() {
        return symbol;
    }

    public Rang symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }
    public Object symbolSize() {
        return symbolSize;
    }

    public Rang symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }
    public Object opacity() {
        return opacity;
    }

    public Rang opacity(Object opacity) {
        this.opacity = opacity;
        return this;
    }
    public Object colorAlpha() {
        return colorAlpha;
    }

    public Rang colorAlpha(Object colorAlpha) {
        this.colorAlpha = colorAlpha;
        return this;
    }
    public Object colorLightness() {
        return colorLightness;
    }

    public Rang colorLightness(Object colorLightness) {
        this.colorLightness = colorLightness;
        return this;
    }
    public Object colorSaturation() {
        return colorSaturation;
    }

    public Rang colorSaturation(Object colorSaturation) {
        this.colorSaturation = colorSaturation;
        return this;
    }
    public Object colorHue() {
        return colorHue;
    }

    public Rang colorHue(Object colorHue) {
        this.colorHue = colorHue;
        return this;
    }
    public Object color() {
        return color;
    }

    public Rang color(Object color) {
        this.color = color;
        return this;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Object getColorAlpha() {
        return colorAlpha;
    }

    public void setColorAlpha(Object colorAlpha) {
        this.colorAlpha = colorAlpha;
    }

    public Object getOpacity() {
        return opacity;
    }

    public void setOpacity(Object opacity) {
        this.opacity = opacity;
    }

    public Object getColorLightness() {
        return colorLightness;
    }

    public void setColorLightness(Object colorLightness) {
        this.colorLightness = colorLightness;
    }

    public Object getColorSaturation() {
        return colorSaturation;
    }

    public void setColorSaturation(Object colorSaturation) {
        this.colorSaturation = colorSaturation;
    }

    public Object getColorHue() {
        return colorHue;
    }

    public void setColorHue(Object colorHue) {
        this.colorHue = colorHue;
    }
}
