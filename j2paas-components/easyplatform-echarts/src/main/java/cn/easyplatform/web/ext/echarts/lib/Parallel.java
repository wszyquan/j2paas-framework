/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.axis.ParallelAxis;
import cn.easyplatform.web.ext.echarts.lib.support.Point;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Parallel extends Point {
    /**
     * 布局方式
     */
    private String layout;
    /**
     * 是否允许点击展开折叠 axis
     * 维度比较多时，比如有 50+ 的维度，那么就会有 50+ 个轴。那么可能会页面显示不下
     */
    private Boolean axisExpandable;
    /**
     * 初始时，以哪个轴为中心展开，这里给出轴的 index。没有默认值，需要手动指定
     */
    private Object axisExpandCenter;
    /**
     * 初始时，有多少个轴会处于展开状态。建议根据你的维度个数而手动指定
     */
    private Integer axisExpandCount;
    /**
     * 在展开状态，轴的间距是多少，单位为像素。
     */
    private Integer axisExpandWidth;
    /**
     * 配置多个 parallelAxis 时，有些值一样的属性，如果书写多遍则比较繁琐，那么可以放置在 parallel.parallelAxisDefault 里。在坐标轴初始化前，parallel.parallelAxisDefault 里的配置项，会分别融合进 parallelAxis，形成最终的坐标轴的配置
     */
    private ParallelAxis parallelAxisDefault;

    private Object width;

    private Object height;

    public Object width() {
        return this.width;
    }

    public Parallel width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Parallel height(Object height) {
        this.height = height;
        return this;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public ParallelAxis parallelAxisDefault() {
        if (parallelAxisDefault == null)
            parallelAxisDefault = new ParallelAxis();
        return parallelAxisDefault;
    }

    public Parallel parallelAxisDefault(ParallelAxis parallelAxisDefault) {
        this.parallelAxisDefault = parallelAxisDefault;
        return this;
    }

    public Integer axisExpandWidth() {
        return axisExpandWidth;
    }

    public Parallel axisExpandWidth(Integer axisExpandWidth) {
        this.axisExpandWidth = axisExpandWidth;
        return this;
    }

    public Integer axisExpandCount() {
        return axisExpandCount;
    }

    public Parallel axisExpandCount(Integer axisExpandCount) {
        this.axisExpandCount = axisExpandCount;
        return this;
    }

    public Object axisExpandCenter() {
        return axisExpandCenter;
    }

    public Parallel axisExpandCenter(Object axisExpandCenter) {
        this.axisExpandCenter = axisExpandCenter;
        return this;
    }

    public Boolean axisExpandable() {
        return axisExpandable;
    }

    public Parallel axisExpandable(Boolean axisExpandable) {
        this.axisExpandable = axisExpandable;
        return this;
    }

    public String layout() {
        return layout;
    }

    public Parallel layout(String layout) {
        this.layout = layout;
        return this;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public Boolean getAxisExpandable() {
        return axisExpandable;
    }

    public void setAxisExpandable(Boolean axisExpandable) {
        this.axisExpandable = axisExpandable;
    }

    public Object getAxisExpandCenter() {
        return axisExpandCenter;
    }

    public void setAxisExpandCenter(Object axisExpandCenter) {
        this.axisExpandCenter = axisExpandCenter;
    }

    public Integer getAxisExpandCount() {
        return axisExpandCount;
    }

    public void setAxisExpandCount(Integer axisExpandCount) {
        this.axisExpandCount = axisExpandCount;
    }

    public Integer getAxisExpandWidth() {
        return axisExpandWidth;
    }

    public void setAxisExpandWidth(Integer axisExpandWidth) {
        this.axisExpandWidth = axisExpandWidth;
    }

    public ParallelAxis getParallelAxisDefault() {
        return parallelAxisDefault;
    }

    public void setParallelAxisDefault(ParallelAxis parallelAxisDefault) {
        this.parallelAxisDefault = parallelAxisDefault;
    }
}
