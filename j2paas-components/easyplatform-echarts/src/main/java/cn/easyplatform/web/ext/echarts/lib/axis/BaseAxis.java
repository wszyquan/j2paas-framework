/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.AxisPointer;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;
import cn.easyplatform.web.ext.echarts.lib.style.NameTextStyle;
import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import cn.easyplatform.web.ext.echarts.lib.type.AxisType;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class BaseAxis implements Serializable {

    /**
     * 坐标轴类型，横轴默认为类目型'category'，纵轴默认为数值型'value'
     *
     * @see AxisType
     */
    private String type;

    /**
     * 坐标轴名称，默认为空
     */
    private String name;
    /**
     * 坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisLine
     * @see LineStyle
     */
    private AxisLine axisLine;
    /**
     * 坐标轴小标记，默认不显示，属性show控制显示与否，属性length控制线长，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisTick
     * @see LineStyle
     */
    private AxisTick axisTick;
    /**
     * 坐标轴文本标签，详见axis.axisLabel
     *
     * @see AxisLabel
     */
    private AxisLabel axisLabel;
    /**
     * 分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see SplitLine
     */
    private SplitLine splitLine;
    /**
     * 分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样式
     */
    private SplitArea splitArea;
    /**
     * 一级层叠控制
     */
    private Integer zlevel;
    /**
     * 二级层叠控制
     */
    private Integer z;

    /**
     * 坐标轴名称与轴线之间的距离
     */
    private Integer nameGap;
    /**
     * 是否是反向坐标轴。ECharts 3 中新加
     */
    private Boolean inverse;
    /**
     * 坐标轴两边留白策略，类目轴和非类目轴的设置和表现不一样
     */
    private Object boundaryGap;
    /**
     * 坐标轴刻度最小值，在类目轴中无效
     */
    private Object min;
    /**
     * 坐标轴刻度最大值，在类目轴中无效
     */
    private Object max;
    /**
     * 只在数值轴中（type: 'value'）有效
     */
    private Boolean scale;
    /**
     * 坐标轴分割间隔
     */
    private Object interval;
    /**
     * 坐标轴的分割段数
     */
    private Integer splitNumber;
    /**
     * 自动计算的坐标轴最小间隔大小
     */
    private Integer minInterval;
    /**
     * 自动计算的坐标轴最大间隔大小。
     */
    private Integer maxInterval;
    /**
     * 坐标轴名称显示位置
     */
    private String nameLocation;
    /**
     * X 轴相对于默认位置的偏移，在相同的 position 上有多个 X 轴的时候有用
     */
    private Integer offset;
    /**
     * 坐标轴名称的文字样式
     */
    private NameTextStyle nameTextStyle;
    /**
     * 坐标轴名字旋转，角度值
     */
    private Integer nameRotate;
    /**
     * 对数轴的底数，只在对数轴中（type: 'log'）有效
     */
    private Integer logBase;
    /**
     * 坐标轴是否是静态无法交互
     */
    private Boolean silent;
    /**
     * 坐标轴的标签是否响应和触发鼠标事件，默认不响应
     */
    private Boolean triggerEvent;
    /**
     * 类目数据，在类目轴（type: 'category'）中有效
     */
    private Object data;

    private AxisPointer axisPointer;

    public AxisPointer axisPointer() {
        return axisPointer;
    }

    public BaseAxis axisPointer(AxisPointer axisPointer) {
        this.axisPointer = axisPointer;
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public BaseAxis silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public Boolean triggerEvent() {
        return triggerEvent;
    }

    public BaseAxis triggerEvent(Boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
        return this;
    }

    public Integer splitNumber() {
        return this.splitNumber;
    }

    public BaseAxis splitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
        return this;
    }

    public Integer offset() {
        return this.offset;
    }

    public BaseAxis offset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public Integer logBase() {
        return this.logBase;
    }

    public BaseAxis logBase(Integer logBase) {
        this.logBase = logBase;
        return this;
    }

    public Integer nameRotate() {
        return this.nameRotate;
    }

    public BaseAxis nameRotate(Integer nameRotate) {
        this.nameRotate = nameRotate;
        return this;
    }

    public Integer minInterval() {
        return this.minInterval;
    }

    public BaseAxis minInterval(Integer minInterval) {
        this.minInterval = minInterval;
        return this;
    }

    public Integer maxInterval() {
        return this.maxInterval;
    }

    public BaseAxis maxInterval(Integer maxInterval) {
        this.maxInterval = maxInterval;
        return this;
    }

    public String nameLocation() {
        return this.nameLocation;
    }

    public BaseAxis nameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
        return this;
    }

    public TextStyle nameTextStyle() {
        if (this.nameTextStyle == null)
            this.nameTextStyle = new NameTextStyle();
        return this.nameTextStyle;
    }

    public BaseAxis nameTextStyle(NameTextStyle nameTextStyle) {
        this.nameTextStyle = nameTextStyle;
        return this;
    }

    public Boolean scale() {
        return this.scale;
    }

    public BaseAxis scale(Boolean scale) {
        this.scale = scale;
        return this;
    }

    public Object interval() {
        return this.interval;
    }

    public BaseAxis interval(Object interval) {
        this.interval = interval;
        return this;
    }

    public BaseAxis interval(Double interval) {
        this.interval = interval;
        return this;
    }

    public Integer nameGap() {
        return this.nameGap;
    }

    public BaseAxis nameGap(Integer nameGap) {
        this.nameGap = nameGap;
        return this;
    }

    public Boolean inverse() {
        return this.inverse;
    }

    public BaseAxis inverse(Boolean inverse) {
        this.inverse = inverse;
        return this;
    }

    public Object boundaryGap() {
        return this.boundaryGap;
    }

    public BaseAxis boundaryGap(Object boundaryGap) {
        this.boundaryGap = boundaryGap;
        return this;
    }

    public BaseAxis boundaryGap(Object[] boundaryGap) {
        this.boundaryGap = boundaryGap;
        return this;
    }

    public BaseAxis boundaryGap(Object o1, Object o2) {
        this.boundaryGap = new Object[]{o1, o2};
        return this;
    }

    public Object min() {
        return this.min;
    }

    public BaseAxis min(Object min) {
        this.min = min;
        return this;
    }

    public BaseAxis min(Double min) {
        this.min = min;
        return this;
    }

    public Object max() {
        return this.max;
    }

    public BaseAxis max(Object max) {
        this.max = max;
        return this;
    }

    public BaseAxis max(Double max) {
        this.max = max;
        return this;
    }

    /**
     * 设置zlevel值
     *
     * @param zlevel
     */
    public BaseAxis zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    /**
     * 获取zlevel值
     */
    public Integer zlevel() {
        return this.zlevel;
    }

    /**
     * 设置z值
     *
     * @param z
     */
    public BaseAxis z(Integer z) {
        this.z = z;
        return this;
    }

    /**
     * 获取z值
     */
    public Integer z() {
        return this.z;
    }

    /**
     * 获取type值
     */
    public String type() {
        return this.type;
    }

    /**
     * 获取type值
     */
    public String getType() {
        return type;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 获取name值
     */
    public String getName() {
        return name;
    }

    /**
     * 设置name值
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    public BaseAxis type(String type) {
        this.type = type;
        return this;

    }

    /**
     * 获取name值
     */
    public String name() {
        return this.name;
    }

    /**
     * 设置name值
     *
     * @param name
     */
    public BaseAxis name(String name) {
        this.name = name;
        return this;
    }

    /**
     * 坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisLine
     * @see LineStyle
     */
    public AxisLine axisLine() {
        if (this.axisLine == null) {
            this.axisLine = new AxisLine();
        }
        return this.axisLine;
    }

    /**
     * 设置axisLine值
     *
     * @param axisLine
     */
    public BaseAxis axisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
        return this;
    }

    /**
     * 坐标轴小标记，默认不显示，属性show控制显示与否，属性length控制线长，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisTick
     * @see LineStyle
     */
    public AxisTick axisTick() {
        if (this.axisTick == null) {
            this.axisTick = new AxisTick();
        }
        return this.axisTick;
    }

    /**
     * 设置axisTick值
     *
     * @param axisTick
     */
    public BaseAxis axisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
        return this;
    }

    /**
     * 坐标轴文本标签，详见axis.axisLabel
     *
     * @see AxisLabel
     */
    public AxisLabel axisLabel() {
        if (this.axisLabel == null) {
            this.axisLabel = new AxisLabel();
        }
        return this.axisLabel;
    }

    /**
     * @param label
     * @return
     */
    public BaseAxis axisLabel(AxisLabel label) {
        this.axisLabel = label;
        return this;
    }


    /**
     * 分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see SplitLine
     */
    public SplitLine splitLine() {
        if (this.splitLine == null) {
            this.splitLine = new SplitLine();
        }
        return this.splitLine;
    }

    /**
     * 设置splitLine值
     *
     * @param splitLine
     */
    public BaseAxis splitLine(SplitLine splitLine) {
        if (this.splitLine == null) {
            this.splitLine = splitLine;
        }
        return this;
    }

    /**
     * 分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样式
     */
    public SplitArea splitArea() {
        if (this.splitArea == null) {
            this.splitArea = new SplitArea();
        }
        return this.splitArea;
    }

    /**
     * 设置splitArea值
     *
     * @param splitArea
     */
    public BaseAxis splitArea(SplitArea splitArea) {
        this.splitArea = splitArea;
        return this;
    }

    /**
     * 添加坐标轴的类目属性
     *
     * @param values
     * @return
     */
    public BaseAxis data(Object... values) {
        this.data = values;
        return this;
    }

    public Object data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public AxisPointer getAxisPointer() {
        return axisPointer;
    }

    public void setAxisPointer(AxisPointer axisPointer) {
        this.axisPointer = axisPointer;
    }

    /**
     * 获取axisLine值
     */
    public AxisLine getAxisLine() {
        return axisLine;
    }

    /**
     * 设置axisLine值
     *
     * @param axisLine
     */
    public void setAxisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
    }

    /**
     * 获取axisTick值
     */
    public AxisTick getAxisTick() {
        return axisTick;
    }

    /**
     * 设置axisTick值
     *
     * @param axisTick
     */
    public void setAxisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
    }

    /**
     * 获取axisLabel值
     */
    public AxisLabel getAxisLabel() {
        return axisLabel;
    }

    /**
     * 设置axisLabel值
     *
     * @param axisLabel
     */
    public void setAxisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
    }

    /**
     * 获取splitLine值
     */
    public SplitLine getSplitLine() {
        return splitLine;
    }

    /**
     * 设置splitLine值
     *
     * @param splitLine
     */
    public void setSplitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
    }

    /**
     * 获取splitArea值
     */
    public SplitArea getSplitArea() {
        return splitArea;
    }

    /**
     * 设置splitArea值
     *
     * @param splitArea
     */
    public void setSplitArea(SplitArea splitArea) {
        this.splitArea = splitArea;
    }

    /**
     * 获取zlevel值
     */
    public Integer getZlevel() {
        return zlevel;
    }

    /**
     * 设置zlevel值
     *
     * @param zlevel
     */
    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    /**
     * 获取z值
     */
    public Integer getZ() {
        return z;
    }

    /**
     * 设置z值
     *
     * @param z
     */
    public void setZ(Integer z) {
        this.z = z;
    }

    public Integer getNameGap() {
        return nameGap;
    }

    public void setNameGap(Integer nameGap) {
        this.nameGap = nameGap;
    }

    public Boolean getInverse() {
        return inverse;
    }

    public void setInverse(Boolean inverse) {
        this.inverse = inverse;
    }

    public Object getBoundaryGap() {
        return boundaryGap;
    }

    public void setBoundaryGap(Object boundaryGap) {
        this.boundaryGap = boundaryGap;
    }

    public Object getMin() {
        return min;
    }

    public void setMin(Object min) {
        this.min = min;
    }

    public Object getMax() {
        return max;
    }

    public void setMax(Object max) {
        this.max = max;
    }

    public Boolean getScale() {
        return scale;
    }

    public void setScale(Boolean scale) {
        this.scale = scale;
    }

    public Object getInterval() {
        return interval;
    }

    public void setInterval(Object interval) {
        this.interval = interval;
    }

    public Integer getSplitNumber() {
        return splitNumber;
    }

    public void setSplitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
    }

    public Integer getMinInterval() {
        return minInterval;
    }

    public void setMinInterval(Integer minInterval) {
        this.minInterval = minInterval;
    }

    public String getNameLocation() {
        return nameLocation;
    }

    public void setNameLocation(String nameLocation) {
        this.nameLocation = nameLocation;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public NameTextStyle getNameTextStyle() {
        return nameTextStyle;
    }

    public void setNameTextStyle(NameTextStyle nameTextStyle) {
        this.nameTextStyle = nameTextStyle;
    }

    public Integer getNameRotate() {
        return nameRotate;
    }

    public void setNameRotate(Integer nameRotate) {
        this.nameRotate = nameRotate;
    }

    public Integer getLogBase() {
        return logBase;
    }

    public void setLogBase(Integer logBase) {
        this.logBase = logBase;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public Boolean getTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(Boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
