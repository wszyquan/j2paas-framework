/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.feature.Brush;
import cn.easyplatform.web.ext.echarts.lib.feature.*;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.IconStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Point;
import cn.easyplatform.web.ext.echarts.lib.type.Tool;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Toolbox extends Point {
    /**
     * 是否显示
     */
    private Boolean show;
    /**
     * 布局方式，默认为水平布局，可选为：'horizontal' | 'vertical'
     */
    private String orient;
    /**
     * 工具箱icon大小，单位（px）
     */
    private Integer itemSize;
    /**
     * 工具栏 icon 每项之间的间隔。横向布局时为水平间隔，纵向布局时为纵向间隔
     */
    private Integer itemGap;
    /**
     * 是否显示工具箱文字提示，默认启用
     */
    private Boolean showTitle;
    /**
     * 启用功能，目前支持feature见下，工具箱自定义功能回调处理
     */
    private Map<String, Feature> feature;
    /**
     * 公用的 icon 样式设置
     */
    private IconStyle iconStyle;

    private Emphasis emphasis;

    private Object width;

    private Object height;

    private Tooltip tooltip;

    public Tooltip tooltip() {
        return this.tooltip;
    }

    public Toolbox tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public Emphasis emphasis() {
        return this.emphasis;
    }

    public Toolbox emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Object width() {
        return this.width;
    }

    public Toolbox width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public Toolbox height(Object height) {
        this.height = height;
        return this;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Boolean show() {
        return this.show;
    }

    public Toolbox show(Boolean show) {
        this.show = show;
        return this;
    }

    public IconStyle iconStyle() {
        return this.iconStyle;
    }

    public Toolbox iconStyle(IconStyle iconStyle) {
        this.iconStyle = iconStyle;
        return this;
    }

    public IconStyle getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(IconStyle iconStyle) {
        this.iconStyle = iconStyle;
    }

    /**
     * 获取orient值
     */
    public String orient() {
        return this.orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public Toolbox orient(String orient) {
        this.orient = orient;
        return this;
    }

    /**
     * 获取itemSize值
     */
    public Integer itemSize() {
        return this.itemSize;
    }

    /**
     * 设置itemSize值
     *
     * @param itemSize
     */
    public Toolbox itemSize(Integer itemSize) {
        this.itemSize = itemSize;
        return this;
    }

    /**
     * 获取showTitle值
     */
    public Boolean showTitle() {
        return this.showTitle;
    }

    /**
     * 设置showTitle值
     *
     * @param showTitle
     */
    public Toolbox showTitle(Boolean showTitle) {
        this.showTitle = showTitle;
        return this;
    }

    /**
     * 启用功能，目前支持feature见下，工具箱自定义功能回调处理
     */
    public Map<String, Feature> feature() {
        if (this.feature == null) {
            this.feature = new LinkedHashMap<String, Feature>();
        }
        return this.feature;
    }

    /**
     * 添加组件
     *
     * @param value
     * @return
     */
    private Toolbox _addFeature(Feature value) {
        if (value == null) {
            return this;
        }
        //第一个字母转小写
        String name = value.getClass().getSimpleName();
        name = name.substring(0, 1).toLowerCase() + name.substring(1);
        _addFeatureOnce(name, value);
        return this;
    }

    /**
     * 添加组件
     *
     * @param values
     * @return
     */
    public Toolbox feature(Object... values) {
        if (values == null && values.length == 0) {
            return this;
        }
        if (this.feature == null) {
            this.feature = new LinkedHashMap<String, Feature>();
        }
        for (Object t : values) {
            if (t instanceof Feature) {
                _addFeature((Feature) t);
            } else if (t instanceof Tool) {
                switch ((Tool) t) {
                    case dataView:
                        _addFeatureOnce(t, new DataView());
                        break;
                    case dataZoom:
                        _addFeatureOnce(t, new DataZoom());
                        break;
                    case magicType:
                        _addFeatureOnce(t, new MagicType());
                        break;
                    case brush:
                        _addFeatureOnce(t, new Brush());
                        break;
                    case restore:
                        _addFeatureOnce(t, new Restore());
                        break;
                    case saveAsImage:
                        _addFeatureOnce(t, new SaveAsImage());
                        break;
                    default:
                        _addFeatureOnce(t, new Mark());
                        //ignore
                }
            }
        }
        return this;
    }

    /**
     * 同一种组件只添加一次
     *
     * @param name
     * @param feature
     */
    private void _addFeatureOnce(Object name, Feature feature) {
        String _name = String.valueOf(name);
        if (!this.feature().containsKey(_name)) {
            this.feature().put(_name, feature);
        }
    }

    /**
     * 获取feature值
     */
    public Map<String, Feature> getFeature() {
        return feature;
    }

    /**
     * 设置feature值
     *
     * @param feature
     */
    public void setFeature(Map<String, Feature> feature) {
        this.feature = feature;
    }

    /**
     * 获取orient值
     */
    public String getOrient() {
        return orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public void setOrient(String orient) {
        this.orient = orient;
    }

    /**
     * 获取itemSize值
     */
    public Integer getItemSize() {
        return itemSize;
    }

    /**
     * 设置itemSize值
     *
     * @param itemSize
     */
    public void setItemSize(Integer itemSize) {
        this.itemSize = itemSize;
    }

    /**
     * 获取showTitle值
     */
    public Boolean getShowTitle() {
        return showTitle;
    }

    /**
     * 设置showTitle值
     *
     * @param showTitle
     */
    public void setShowTitle(Boolean showTitle) {
        this.showTitle = showTitle;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Integer getItemGap() {
        return itemGap;
    }

    public void setItemGap(Integer itemGap) {
        this.itemGap = itemGap;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }
}
