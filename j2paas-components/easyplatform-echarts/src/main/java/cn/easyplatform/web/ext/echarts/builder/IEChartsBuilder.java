/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder;

import cn.easyplatform.web.ext.ComponentHandler;
import cn.easyplatform.web.ext.echarts.ECharts;
import cn.easyplatform.web.ext.echarts.lib.Option;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IEChartsBuilder {


    /**
     * 开发模式,创建默认的图表
     *
     * @param option
     * @param item
     * @param clean
     */
    String build(Option option, TemplateUnit item, boolean clean);

    /**
     * 获取对应类型的所有模板
     * @return
     */
    Collection<TemplateUnit> getTemplates();

    /**
     * 获取模板信息
     *
     * @param name
     * @return
     */
    TemplateUnit getTemplate(String name);

    /**
     * 获取创建图表时对部分图表的定制条件
     * @return
     */
    Map<String , List > getCondition();

    /**
     * 运行时模式，根据给指定的数据生成图表
     *
     * @param charts
     * @param dataHandler
     */
    void build(ECharts charts, ComponentHandler dataHandler);
}
