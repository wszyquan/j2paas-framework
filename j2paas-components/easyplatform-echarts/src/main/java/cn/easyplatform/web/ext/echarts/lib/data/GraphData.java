/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GraphData extends Category {

    private static final long serialVersionUID = 7218201600361155091L;

    private Integer category;

    private Object value;

    private Object x;

    private  Object y;

    private Object id;

    private Boolean draggable;

    public GraphData(String name) {
        super(name);
    }

    public GraphData(String name, Object value) {
        super(name);
        this.value = value;
    }
    public Boolean draggable() {
        return this.draggable;
    }

    public GraphData draggable(Boolean draggable) {
        this.draggable = draggable;
        return this;
    }

    public Object x() {
        return this.x;
    }

    public GraphData x(Object x) {
        this.x = x;
        return this;
    }
    public Object y() {
        return this.y;
    }

    public GraphData y(Object y) {
        this.y = y;
        return this;
    }
    public Object id() {
        return this.id;
    }

    public GraphData id(Object id) {
        this.id = id;
        return this;
    }
    public Integer category() {
        return this.category;
    }

    public GraphData category(Integer category) {
        this.category = category;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public GraphData value(Object value) {
        this.value = value;
        return this;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getX() {
        return x;
    }

    public void setX(Object x) {
        this.x = x;
    }

    public Object getY() {
        return y;
    }

    public void setY(Object y) {
        this.y = y;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Boolean getDraggable() {
        return draggable;
    }

    public void setDraggable(Boolean draggable) {
        this.draggable = draggable;
    }
}
