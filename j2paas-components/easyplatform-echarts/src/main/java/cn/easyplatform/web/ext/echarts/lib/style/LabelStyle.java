/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LabelStyle extends TextStyle {
    /**
     * 标题背景颜色，默认透明
     */
    private String backgroundColor;
    /**
     * 标题边框颜色
     */
    private String borderColor;
    /**
     * borderWidth
     */
    private Integer borderWidth;
    /**
     * 圆角半径，单位px，支持传入数组分别指定 4 个圆角半径
     */
    private Object borderRadius;
    /**
     * 图形阴影的模糊大小。该属性配合 shadowColor,shadowOffsetX, shadowOffsetY 一起设置图形的阴影效果
     */
    private Integer shadowBlur;
    /**
     * 阴影颜色
     */
    private String shadowColor;
    /**
     * 阴影水平方向上的偏移距离
     */
    private Integer shadowOffsetX;
    /**
     * 阴影垂直方向上的偏移距离
     */
    private Integer shadowOffsetY;
    /**
     * 文字垂直对齐方式，默认自动。
     */
    private String verticalAlign;
    /**
     * label 距离轴的距离。
     */
    private Integer margin;
    /**
     * 文本标签文字的格式化器。
     */
    private Object formatter;
    /**
     * 文本标签中数值的小数点精度。默认根据当前轴的值自动判断。也可以指定如 2 表示保留两位小数。
     */
    private Object precision;
    /**
     * 是否显示 label。
     */
    private Boolean show;

    /**
     * label 的间隔。当指定为数值时，例如指定为 2，则每隔两个显示一个 label
     */
    private Object interval;
    /**
     * label 的旋转角度。正值表示逆时针旋转。
     */
    private Object rotate;

    /**
     * 可选的配置方式：
     * <p>
     * 'auto'： 完全自动决定。
     * <p>
     * 'left'： 贴左边界放置。 当 timline.orient 为 'vertical' 时有效。
     * <p>
     * 'right'：当 timline.orient 为 'vertical' 时有效。 贴右边界放置。
     * <p>
     * 'top'： 贴上边界放置。 当 timline.orient 为 'horizontal' 时有效。
     * <p>
     * 'bottom'： 贴下边界放置。 当 timline.orient 为 'horizontal' 时有效。
     * <p>
     * number： 指定某个数值时，表示 label 和轴的距离。为 0 时则和坐标轴重合，可以为正负值，决定 label 在坐标轴的哪一边
     */
    private Object position;
    /**
     * 距离图形元素的距离。当 position 为字符描述值（如 'top'、'insideRight'）时候有效。
     */
    private Integer distance;

    /**
     * 是否对文字进行偏移。默认不偏移。例如：[30, 40] 表示文字在横向上偏移 30，纵向上偏移 40。
     */
    private Object offset;

    /**
     * 文字水平对齐方式，默认自动。
     */
    private String align;
    /**
     * 当某个扇形块的角度小于该值（角度制）时，扇形块对应的文字不显示。该值用以隐藏过小扇形块中的文字
     */
    private Integer minAngle;
    /**
     * 文字块的内边距
     */
    private Object padding;

    private Emphasis emphasis;

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public LabelStyle emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Object padding() {
        return this.padding;
    }

    public LabelStyle padding(Object padding) {
        this.padding = padding;
        return this;
    }

    public Integer minAngle() {
        return this.minAngle;
    }

    public LabelStyle minAngle(Integer minAngle) {
        this.minAngle = minAngle;
        return this;
    }

    public String align() {
        return this.align;
    }

    public LabelStyle align(String align) {
        this.align = align;
        return this;
    }

    public Object offset() {
        return this.offset;
    }

    public LabelStyle offset(Object offset) {
        this.offset = offset;
        return this;
    }

    public Integer distance() {
        return this.distance;
    }

    public LabelStyle distance(Integer distance) {
        this.distance = distance;
        return this;
    }

    public Object rotate() {
        return this.show;
    }

    public LabelStyle rotate(Object rotate) {
        this.rotate = rotate;
        return this;
    }

    public Object interval() {
        return this.show;
    }

    public LabelStyle interval(Object interval) {
        this.interval = interval;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public LabelStyle show(Boolean show) {
        this.show = show;
        return this;
    }

    public Object precision() {
        return this.precision;
    }

    public LabelStyle precision(Object precision) {
        this.precision = precision;
        return this;
    }

    public Object formatter() {
        return this.formatter;
    }

    public LabelStyle formatter(Object formatter) {
        this.formatter = formatter;
        return this;
    }

    public Integer margin() {
        return this.margin;
    }

    public LabelStyle margin(Integer margin) {
        this.margin = margin;
        return this;
    }

    public String verticalAlign() {
        return this.verticalAlign;
    }

    public LabelStyle verticalAlign(String verticalAlign) {
        this.verticalAlign = verticalAlign;
        return this;
    }

    public Object borderRadius() {
        return this.borderRadius;
    }

    public LabelStyle borderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;
        return this;
    }

    public String backgroundColor() {
        return this.backgroundColor;
    }

    public LabelStyle backgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public String borderColor() {
        return this.borderColor;
    }

    public LabelStyle borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public LabelStyle borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Integer shadowBlur() {
        return this.shadowBlur;
    }

    public LabelStyle shadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
        return this;
    }

    public String shadowColor() {
        return this.shadowColor;
    }

    public LabelStyle shadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
        return this;
    }

    public Integer shadowOffsetX() {
        return this.shadowOffsetX;
    }

    public LabelStyle shadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
        return this;
    }

    public Integer shadowOffsetY() {
        return this.shadowOffsetY;
    }

    public LabelStyle shadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
        return this;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Object getBorderRadius() {
        return borderRadius;
    }

    public void setBorderRadius(Object borderRadius) {
        this.borderRadius = borderRadius;
    }

    public Integer getShadowBlur() {
        return shadowBlur;
    }

    public void setShadowBlur(Integer shadowBlur) {
        this.shadowBlur = shadowBlur;
    }

    public String getShadowColor() {
        return shadowColor;
    }

    public void setShadowColor(String shadowColor) {
        this.shadowColor = shadowColor;
    }

    public Integer getShadowOffsetX() {
        return shadowOffsetX;
    }

    public void setShadowOffsetX(Integer shadowOffsetX) {
        this.shadowOffsetX = shadowOffsetX;
    }

    public Integer getShadowOffsetY() {
        return shadowOffsetY;
    }

    public void setShadowOffsetY(Integer shadowOffsetY) {
        this.shadowOffsetY = shadowOffsetY;
    }

    public String getVerticalAlign() {
        return verticalAlign;
    }

    public void setVerticalAlign(String verticalAlign) {
        this.verticalAlign = verticalAlign;
    }

    public Integer getMargin() {
        return margin;
    }

    public void setMargin(Integer margin) {
        this.margin = margin;
    }

    public Object getFormatter() {
        return formatter;
    }

    public void setFormatter(Object formatter) {
        this.formatter = formatter;
    }

    public Object getPrecision() {
        return precision;
    }

    public void setPrecision(Object precision) {
        this.precision = precision;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Object getInterval() {
        return interval;
    }

    public void setInterval(Object interval) {
        this.interval = interval;
    }

    public Object getRotate() {
        return rotate;
    }

    public void setRotate(Object rotate) {
        this.rotate = rotate;
    }

    public Object getPosition() {
        return position;
    }

    public void setPosition(Object position) {
        this.position = position;
    }

    public Object position() {
        return position;
    }

    public LabelStyle position(Object position) {
        this.position = position;
        return this;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Object getOffset() {
        return offset;
    }

    public void setOffset(Object offset) {
        this.offset = offset;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }
}
