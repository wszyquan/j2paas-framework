/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.axis.*;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;
import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 雷达图坐标系组件，只适用于雷达图
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Radar extends Polar {

    /**
     * 开始角度, 有效输入范围：[-180,180]
     */
    private Integer startAngle;
    /**
     * 坐标轴名称
     */
    private Name name;
    /**
     * 坐标轴名称与轴线之间的距离
     */
    private Integer nameGap;
    /**
     * 分割段数，默认为5
     */
    private Integer splitNumber;
    /**
     * 极坐标的形状，'polygon'|'circle' 多边形|圆形
     */
    private Object shape;
    /**
     * 脱离0值比例，放大聚焦到最终_min，_max区间
     */
    private Boolean scale;
    /**
     * 坐标轴是否是静态无法交互
     */
    private Boolean silent;
    /**
     * 坐标轴的标签是否响应和触发鼠标事件，默认不响应
     */
    private Boolean triggerEvent;
    /**
     * 坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisLine
     */
    private AxisLine axisLine;
    /**
     * 坐标轴小标记，默认不显示，属性show控制显示与否，属性length控制线长，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisTick
     * @see LineStyle
     */
    private AxisTick axisTick;
    /**
     * 坐标轴文本标签，详见axis.axisLabel
     *
     * @see AxisLabel
     */
    private AxisLabel axisLabel;
    /**
     * 分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样式
     *
     * @see SplitArea
     */
    private SplitArea splitArea;
    /**
     * 分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see SplitLine
     */
    private SplitLine splitLine;

    /**
     * 雷达指标列表，同时也是label内容
     */
    private List<Indicator> indicator;

    public Boolean silent() {
        return silent;
    }

    public Radar silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public Boolean triggerEvent() {
        return triggerEvent;
    }

    public Radar triggerEvent(Boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
        return this;
    }

    /**
     * 坐标轴小标记，默认不显示，属性show控制显示与否，属性length控制线长，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisTick
     * @see LineStyle
     */
    public AxisTick axisTick() {
        if (this.axisTick == null) {
            this.axisTick = new AxisTick();
        }
        return this.axisTick;
    }

    /**
     * 设置axisTick值
     *
     * @param axisTick
     */
    public Radar axisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
        return this;
    }

    public Integer nameGap() {
        return this.nameGap;
    }

    public Radar nameGap(Integer nameGap) {
        this.nameGap = nameGap;
        return this;
    }

    /**
     * 设置name值
     *
     * @param name
     */
    public Radar name(Name name) {
        this.name = name;
        return this;
    }

    /**
     * 设置axisLine值
     *
     * @param axisLine
     */
    public Radar axisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
        return this;
    }

    /**
     * 设置axisLabel值
     *
     * @param axisLabel
     */
    public Radar axisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
        return this;
    }

    /**
     * 设置splitArea值
     *
     * @param splitArea
     */
    public Radar splitArea(SplitArea splitArea) {
        this.splitArea = splitArea;
        return this;
    }

    /**
     * 设置splitLine值
     *
     * @param splitLine
     */
    public Radar splitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
        return this;
    }

    /**
     * 设置indicator值
     *
     * @param indicator
     */
    public Radar indicator(Indicator... indicator) {
        this.indicator = Arrays.asList(indicator);
        return this;
    }

    /**
     * 获取startAngle值
     */
    public Integer startAngle() {
        return this.startAngle;
    }

    /**
     * 设置startAngle值
     *
     * @param startAngle
     */
    public Radar startAngle(Integer startAngle) {
        this.startAngle = startAngle;
        return this;
    }

    /**
     * 获取splitNumber值
     */
    public Integer splitNumber() {
        return this.splitNumber;
    }

    /**
     * 设置splitNumber值
     *
     * @param splitNumber
     */
    public Radar splitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
        return this;
    }

    /**
     * 坐标轴名称
     */
    public Name name() {
        if (this.name == null)
            this.name = new Name();
        return this.name;
    }

    /**
     * 获取scale值
     */
    public Boolean scale() {
        return this.scale;
    }

    /**
     * 设置scale值
     *
     * @param scale
     */
    public Radar scale(Boolean scale) {
        this.scale = scale;
        return this;
    }

    /**
     * 坐标轴线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see AxisLine
     */
    public AxisLine axisLine() {
        if (this.axisLine == null) {
            this.axisLine = new AxisLine();
        }
        return this.axisLine;
    }

    /**
     * 坐标轴文本标签，详见axis.axisLabel
     *
     * @see AxisLabel
     */
    public AxisLabel axisLabel() {
        if (this.axisLabel == null) {
            this.axisLabel = new AxisLabel();
        }
        return this.axisLabel;
    }

    /**
     * 分隔区域，默认不显示，属性show控制显示与否，属性areaStyle（详见areaStyle）控制区域样式
     *
     * @see SplitArea
     */
    public SplitArea splitArea() {
        if (this.splitArea == null) {
            this.splitArea = new SplitArea();
        }
        return this.splitArea;
    }

    /**
     * 分隔线，默认显示，属性show控制显示与否，属性lineStyle（详见lineStyle）控制线条样式
     *
     * @see SplitLine
     */
    public SplitLine splitLine() {
        if (this.splitLine == null) {
            this.splitLine = new SplitLine();
        }
        return this.splitLine;
    }

    public Object shape() {
        return this.shape;
    }

    public Radar shape(Object shape) {
        this.shape = shape;
        return this;
    }

    /**
     * 雷达指标列表，同时也是label内容
     */
    public Indicator indicator() {
        if (this.indicator == null)
            this.indicator = new ArrayList<Indicator>();
        Indicator indi = new Indicator();
        this.indicator.add(indi);
        return indi;
    }

    /**
     * 雷达指标列表，同时也是label内容
     */
    public Radar indicator(String name, Object max) {
        if (this.indicator == null)
            this.indicator = new ArrayList<Indicator>();
        Indicator indi = new Indicator();
        indi.setMax(max);
        indi.setName(name);
        this.indicator.add(indi);
        return this;
    }

    /**
     * 获取name值
     */
    public Name getName() {
        return name;
    }

    /**
     * 设置name值
     *
     * @param name
     */
    public void setName(Name name) {
        this.name = name;
    }

    /**
     * 获取axisLine值
     */
    public AxisLine getAxisLine() {
        return axisLine;
    }

    /**
     * 设置axisLine值
     *
     * @param axisLine
     */
    public void setAxisLine(AxisLine axisLine) {
        this.axisLine = axisLine;
    }

    /**
     * 获取axisLabel值
     */
    public AxisLabel getAxisLabel() {
        return axisLabel;
    }

    /**
     * 设置axisLabel值
     *
     * @param axisLabel
     */
    public void setAxisLabel(AxisLabel axisLabel) {
        this.axisLabel = axisLabel;
    }

    /**
     * 获取splitArea值
     */
    public SplitArea getSplitArea() {
        return splitArea;
    }

    /**
     * 设置splitArea值
     *
     * @param splitArea
     */
    public void setSplitArea(SplitArea splitArea) {
        this.splitArea = splitArea;
    }

    /**
     * 获取splitLine值
     */
    public SplitLine getSplitLine() {
        return splitLine;
    }

    /**
     * 设置splitLine值
     *
     * @param splitLine
     */
    public void setSplitLine(SplitLine splitLine) {
        this.splitLine = splitLine;
    }

    /**
     * 获取indicator值
     */
    public List<Indicator> getIndicator() {
        return indicator;
    }

    /**
     * 设置indicator值
     *
     * @param indicator
     */
    public void setIndicator(List<Indicator> indicator) {

        this.indicator = indicator;
    }

    /**
     * 获取startAngle值
     */
    public Integer getStartAngle() {
        return startAngle;
    }

    /**
     * 设置startAngle值
     *
     * @param startAngle
     */
    public void setStartAngle(Integer startAngle) {
        this.startAngle = startAngle;
    }

    /**
     * 获取splitNumber值
     */
    public Integer getSplitNumber() {
        return splitNumber;
    }

    /**
     * 设置splitNumber值
     *
     * @param splitNumber
     */
    public void setSplitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
    }

    /**
     * 获取scale值
     */
    public Boolean getScale() {
        return scale;
    }

    /**
     * 设置scale值
     *
     * @param scale
     */
    public void setScale(Boolean scale) {
        this.scale = scale;
    }

    public Integer getNameGap() {
        return nameGap;
    }

    public void setNameGap(Integer nameGap) {
        this.nameGap = nameGap;
    }

    public Object getShape() {
        return shape;
    }

    public void setShape(Object shape) {
        this.shape = shape;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public Boolean getTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(Boolean triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public AxisTick getAxisTick() {
        return axisTick;
    }

    public void setAxisTick(AxisTick axisTick) {
        this.axisTick = axisTick;
    }

    public static class Indicator {

        private String name;

        private Object max;

        private Object min;

        private String color;

        public String color() {
            return this.color;
        }

        public Indicator color(String color) {
            this.color = color;
            return this;
        }

        public Object min() {
            return this.min;
        }

        public Indicator min(Object min) {
            this.min = min;
            return this;
        }

        public String name() {
            return this.name;
        }

        public Indicator name(String name) {
            this.name = name;
            return this;
        }

        public Object max() {
            return this.max;
        }

        public Indicator max(Object max) {
            this.max = max;
            return this;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getMax() {
            return max;
        }

        public void setMax(Object max) {
            this.max = max;
        }
    }

    public static class Name extends TextStyle {
        private Boolean show;
        private String formatter;

        /**
         * 构造函数
         */
        public Name() {
            this.show(true);
            color("#333");
        }

        /**
         * 获取show值
         */
        public Boolean show() {
            return this.show;
        }

        /**
         * 设置show值
         *
         * @param show
         */
        public Name show(Boolean show) {
            this.show = show;
            return this;
        }

        /**
         * 获取formatter值
         */
        public String formatter() {
            return this.formatter;
        }

        /**
         * 设置formatter
         *
         * @param formatter
         */
        public Name formatter(String formatter) {
            this.formatter = formatter;
            return this;
        }


        /**
         * 获取show值
         */
        public Boolean getShow() {
            return show;
        }

        /**
         * 设置show值
         *
         * @param show
         */
        public void setShow(Boolean show) {
            this.show = show;
        }

        public String getFormatter() {
            return formatter;
        }

        public void setFormatter(String formatter) {
            this.formatter = formatter;
        }
    }
}
