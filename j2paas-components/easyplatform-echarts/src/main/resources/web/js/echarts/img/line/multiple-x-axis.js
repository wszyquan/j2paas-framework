{
    color: ['#5793f3', '#d14a61', '#675bba'],

    tooltip: {
        trigger: 'none',
        axisPointer: {
            type: 'cross'
        }
    },
    legend: {
        data:['2015 降水量', '2016 降水量']
    },
    grid: {
        top: 70,
        bottom: 50
    },
    xAxis: [
        {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            axisLine: {
                onZero: false,
                lineStyle: {
                    color: '#d14a61'
                }
            },
            axisPointer: {
                label: {
                    formatter: "function(params){return '降水量  ' + params.value+ (params.seriesData.length ? '：' + params.seriesData[0].data : '')}"
                }
            },
            data: 'data'
        },
        {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            axisLine: {
                onZero: false,
                lineStyle: {
                    color: '#5793f3'
                }
            },
            axisPointer: {
                label: {
                    formatter: "function(params){return '降水量  ' + params.value+ (params.seriesData.length ? '：' + params.seriesData[0].data : '')}"
                }
            },
            data: 'data'
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name: '2015 降水量',
            type: 'line',
            xAxisIndex: 1,
            smooth: true,
            data: 'data'
        },
        {
            name: '2016 降水量',
            type: 'line',
            smooth: true,
            data: 'data'
        }
    ]
}