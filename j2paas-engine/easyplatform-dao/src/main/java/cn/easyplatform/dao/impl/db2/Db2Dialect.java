/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao.impl.db2;

import cn.easyplatform.dao.Page;
import cn.easyplatform.dao.dialect.Dialect;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Db2Dialect extends Dialect {

	@Override
	protected String getPageBefore(Page page) {
		return "SELECT * FROM (SELECT ROW_NUMBER() OVER() AS ROWNUM, T.* FROM (";
	}

	@Override
	protected String getPageAfter(Page page) {
		int start = (page.getPageNo() - 1) * page.getPageSize() + 1;
		return String.format(") T) AS A WHERE ROWNUM BETWEEN %d AND %d", start,
				start + page.getPageSize());
	}

}
