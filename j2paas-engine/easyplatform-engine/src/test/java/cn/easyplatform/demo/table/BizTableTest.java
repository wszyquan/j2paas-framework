/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.table;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.DaoFactory;
import cn.easyplatform.dao.SeqDao;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.demo.table.expert.TableExpertFactory;
import cn.easyplatform.dos.SerialDo;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.sql.*;


/**
 * 设定一个场景：</br> 商品表goods</br> 商品类别category</br> 客户表customer</br>
 * 客户定单表cust_order</br>
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BizTableTest extends AbstractDemoTest {

	public void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在新建demo业务表");
			conn = JdbcTransactions.getConnection(ds1);
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "goods");
			pstmt.setString(2, "商品档");
			pstmt.setString(3, "商品档");
			pstmt.setString(4, EntityType.TABLE.getName());
			String goods_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("goods.xml")));
			pstmt.setString(5, goods_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "category");
			pstmt.setString(2, "商品分类档");
			pstmt.setString(3, "商品分类档");
			pstmt.setString(4, EntityType.TABLE.getName());
			String category_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream("category.xml")));
			pstmt.setString(5, category_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "customer");
			pstmt.setString(2, "客户档");
			pstmt.setString(3, "客户档");
			pstmt.setString(4, EntityType.TABLE.getName());
			String customer_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream("customer.xml")));
			pstmt.setString(5, customer_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "customer_bak");
			pstmt.setString(2, "客户档备份档");
			pstmt.setString(3, "客户档备份档");
			pstmt.setString(4, EntityType.TABLE.getName());
			String customer_bak_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream("customer_bak.xml")));
			pstmt.setString(5, customer_bak_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "cust_order");
			pstmt.setString(2, "用户订单");
			pstmt.setString(3, "用户订单");
			pstmt.setString(4, EntityType.TABLE.getName());
			String order_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("cust_order.xml")));
			pstmt.setString(5, order_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "order_detail");
			pstmt.setString(2, "订单明细");
			pstmt.setString(3, "订单明细");
			pstmt.setString(4, EntityType.TABLE.getName());
			String order_detail = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("order_detail.xml")));
			pstmt.setString(5, order_detail);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "user_leave");
			pstmt.setString(2, "用户申假表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String user_leave = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("user_leave.xml")));
			pstmt.setString(5, user_leave);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			// 工作流
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_order");
			pstmt.setString(2, "流程实例表");
			pstmt.setString(3, "流程实例表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_order = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_order.xml")));
			pstmt.setString(5, wf_order);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_task");
			pstmt.setString(2, "流程任务表");
			pstmt.setString(3, "流程任务表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_task = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_task.xml")));
			pstmt.setString(5, wf_task);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_task_actor");
			pstmt.setString(2, "任务参与者表");
			pstmt.setString(3, "任务参与者表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_task_actor = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"wf_task_actor.xml")));
			pstmt.setString(5, wf_task_actor);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_hist_order");
			pstmt.setString(2, "历史流程实例表");
			pstmt.setString(3, "历史流程实例表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_hist_order = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"wf_hist_order.xml")));
			pstmt.setString(5, wf_hist_order);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_hist_task");
			pstmt.setString(2, "历史任务表");
			pstmt.setString(3, "历史任务表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_hist_task = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_hist_task.xml")));
			pstmt.setString(5, wf_hist_task);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_hist_task_actor");
			pstmt.setString(2, "历史任务参与者表");
			pstmt.setString(3, "历史任务参与者表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_hist_task_actor = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"wf_hist_task_actor.xml")));
			pstmt.setString(5, wf_hist_task_actor);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "wf_cc_order");
			pstmt.setString(2, "抄送实例表");
			pstmt.setString(3, "抄送实例表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String wf_cc_order = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("wf_cc_order.xml")));
			pstmt.setString(5, wf_cc_order);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			log.debug("新建demo业务表成功");

		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("BizTable", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}

	public void test2() {
		log.debug("正在创建demo业务表");
		SeqDao dao = DaoFactory.createSeqDao(ds2);
		TableBean tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream("goods.xml")));
		tb.setId("goods");
		createTable(tb, dao);
		tb = TransformerFactory.newInstance().transformFromXml(TableBean.class,
				Streams.buff(getClass().getResourceAsStream("category.xml")));
		tb.setId("category");
		createTable(tb, dao);
		tb = TransformerFactory.newInstance().transformFromXml(TableBean.class,
				Streams.buff(getClass().getResourceAsStream("customer.xml")));
		tb.setId("customer");
		createTable(tb, dao);
		tb = TransformerFactory.newInstance().transformFromXml(TableBean.class,
				Streams.buff(getClass().getResourceAsStream("cust_order.xml")));
		tb.setId("cust_order");
		createTable(tb, dao);
		tb = TransformerFactory.newInstance()
				.transformFromXml(
						TableBean.class,
						Streams.buff(getClass().getResourceAsStream(
								"order_detail.xml")));
		tb.setId("order_detail");
		createTable(tb, dao);
		tb = TransformerFactory.newInstance().transformFromXml(TableBean.class,
				Streams.buff(getClass().getResourceAsStream("user_leave.xml")));
		tb.setId("user_leave");
		createTable(tb, dao);

		// 工作流
		tb = TransformerFactory.newInstance().transformFromXml(TableBean.class,
				Streams.buff(getClass().getResourceAsStream("wf_order.xml")));
		tb.setId("wf_order");
		createTable(tb, dao);

		tb = TransformerFactory.newInstance().transformFromXml(TableBean.class,
				Streams.buff(getClass().getResourceAsStream("wf_task.xml")));
		tb.setId("wf_task");
		createTable(tb, dao);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("wf_task_actor.xml")));
		tb.setId("wf_task_actor");
		createTable(tb, dao);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("wf_hist_order.xml")));
		tb.setId("wf_hist_order");
		createTable(tb, dao);

		tb = TransformerFactory.newInstance()
				.transformFromXml(
						TableBean.class,
						Streams.buff(getClass().getResourceAsStream(
								"wf_hist_task.xml")));
		tb.setId("wf_hist_task");
		createTable(tb, dao);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"wf_hist_task_actor.xml")));
		tb.setId("wf_hist_task_actor");
		createTable(tb, dao);

		tb = TransformerFactory.newInstance()
				.transformFromXml(
						TableBean.class,
						Streams.buff(getClass().getResourceAsStream(
								"wf_cc_order.xml")));
		tb.setId("wf_cc_order");
		createTable(tb, dao);

		log.debug("创建demo业务表成功");
	}

	private void createTable(TableBean table, SeqDao dao) {
		Connection conn = null;
		Statement stmt = null;
		try {
			log.debug("createTable:" + table.getId());
			conn = JdbcTransactions.getConnection(ds2);
//			stmt = conn.createStatement();
//			String sql = "drop table if EXISTS " + table.getTableName();
//			if (DaoUtils.getDbType(ds2.toString()) == DaoUtils.SYBASE)
//				sql = "if exists(select 1 from sysobjects where name='"
//						+ table.getTableName()
//						+ "') drop table "
//						+ table.getTableName();
//			stmt.execute(sql);
//			DaoUtils.closeQuietly(stmt);
			stmt = conn.createStatement();
			stmt.execute(TableExpertFactory.createExpert(
					DaoUtils.getDbType(ds2.toString())).createSql(table));
			//if (table.isAutoKey())
			//	dao.create();
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new EasyPlatformRuntimeException("BizTable", ex);
		} finally {
			DaoUtils.closeQuietly(stmt);
		}
	}
}
