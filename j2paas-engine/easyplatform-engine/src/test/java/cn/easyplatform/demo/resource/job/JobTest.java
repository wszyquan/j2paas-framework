/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.resource.job;

import cn.easyplatform.demo.AbstractDemoTest;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JobTest extends AbstractDemoTest {

	/**
	 * Demo系统
	 * 
	 * @throws Exception
	 */
	public void test1() {
//		Connection conn = null;
//		PreparedStatement pstmt = null;
//		try {
//			log.debug("正在新建计划任务");
//			conn = JdbcTransactions.getConnection(ds1);
//			pstmt = conn
//					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
//			pstmt.setString(1, "demo.job.1");
//			pstmt.setString(2, "跑BT");
//			pstmt.setString(3, "");
//			pstmt.setString(4, EntityType.RESOURCE.getName());
//			pstmt.setString(5, SubType.JOB.getName());
//			String job1 = Streams.readAndClose(Streams.utf8r(getClass()
//					.getResourceAsStream("job1.xml")));
//			pstmt.setString(6, job1);
//			pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
//			pstmt.setString(8, "admin");
//			pstmt.setString(9, "C");
//			pstmt.execute();
//			log.debug("新建计划任务成功");
//		} catch (SQLException ex) {
//			throw new GraniteRuntimeException("DataSourceTest", ex);
//		} finally {
//			DaoUtils.closeQuietly(pstmt);
//		}
	}
}
