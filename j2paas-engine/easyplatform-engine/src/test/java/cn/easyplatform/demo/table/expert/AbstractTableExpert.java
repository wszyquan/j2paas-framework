/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.table.expert;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Lang;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AbstractTableExpert implements ITableExpert {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.rjit.granite.demo.table.ITableExpert#create(com.rjit.granite.entities
	 * .beans.table.TableBean)
	 */
	@Override
	public String createSql(TableBean tb) {
		StringBuilder sb = new StringBuilder("CREATE TABLE "
				+ tb.getId() + "(");
		for (TableField tf : tb.getFields()) {
			sb.append('\n').append(tf.getName());
			sb.append(' ').append(evalFieldType(tf));
			if (tf.isNotNull())
				sb.append(" NOT NULL");
			else 
				sb.append(" NULL");
			if (tf.getDefaultValue() != null)
				sb.append(" DEFAULT '").append(tf.getDefaultValue())
						.append('\'');
			sb.append(',');
		}
		if (tb.getKey() != null && !tb.getKey().isEmpty()) {
			sb.append(" PRIMARY KEY (");
			for (String field : tb.getKey()) {
				sb.append(field).append(",");
			}
			sb.setCharAt(sb.length() - 1, ')');
		} else
			sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		return sb.toString();
	}

	protected String evalFieldType(TableField mf) {
		switch (mf.getType()) {
		case CHAR:
			return "CHAR(" + mf.getLength() + ")";

		case BOOLEAN:
			return "BOOLEAN";

		case VARCHAR:
			return "VARCHAR(" + mf.getLength() + ")";

		case CLOB:
			return "TEXT";

		case OBJECT:
		case BLOB:
			return "BLOB";

		case DATETIME:
			return "TIMESTAMP";

		case DATE:
			return "DATE";
		case TIME:
			return "TIME";
		case LONG:
		case INT:
			// 用户自定义了宽度
			if (mf.getLength() > 0)
				return "INT(" + mf.getLength() + ")";
			// 用数据库的默认宽度
			return "INT";

		case NUMERIC:
			// 用户自定义了精度
			if (mf.getLength() > 0 && mf.getDecimal() > 0) {
				return "NUMERIC(" + mf.getLength() + "," + mf.getDecimal()
						+ ")";
			}
			return "NUMERIC(15,2)";
		default:
			throw Lang.makeThrow("Unsupport colType '%s' of field '%s' ",
					mf.getType(), mf.getName());
		}
	}
}
