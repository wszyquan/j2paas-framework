/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.table;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.ServiceException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.demo.table.expert.TableExpertFactory;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;

import java.io.Reader;
import java.sql.*;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SysTableTest extends AbstractDemoTest {
	public void test1() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			log.debug("正在新建demo系统表");
			conn = JdbcTransactions.getConnection(ds1);
			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_user_info");
			pstmt.setString(2, "用户表");
			pstmt.setString(3, "用户表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String user_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("sys_user_info.xml")));
			pstmt.setString(5, user_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_role_info");
			pstmt.setString(2, "角色表");
			pstmt.setString(3, "角色表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String role_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("sys_role_info.xml")));
			pstmt.setString(5, role_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_user_role_info");
			pstmt.setString(2, "用户角色表");
			pstmt.setString(3, "用户角色表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String user_role_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_user_role_info.xml")));
			pstmt.setString(5, user_role_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_menu_info");
			pstmt.setString(2, "菜单");
			pstmt.setString(3, "菜单");
			pstmt.setString(4, EntityType.TABLE.getName());
			String menu_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("sys_menu_info.xml")));
			pstmt.setString(5, menu_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_role_menu_info");
			pstmt.setString(2, "角色菜单");
			pstmt.setString(3, "角色菜单");
			pstmt.setString(4, EntityType.TABLE.getName());
			String role_menu_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_role_menu_info.xml")));
			pstmt.setString(5, role_menu_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_org_info");
			pstmt.setString(2, "组织机构表");
			pstmt.setString(3, "组织机构表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String org_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("sys_org_info.xml")));
			pstmt.setString(5, org_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_user_org_info");
			pstmt.setString(2, "用户机构表");
			pstmt.setString(3, "用户机构表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String user_org_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_user_org_info.xml")));
			pstmt.setString(5, user_org_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_message_info");
			pstmt.setString(2, "系统多国语言信息表");
			pstmt.setString(3, "系统多国语言信息表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String message_info = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("sys_message_info.xml")));
			pstmt.setString(5, message_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_agent_info");
			pstmt.setString(2, "用户委托代理表");
			pstmt.setString(3, "用户委托代理表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String user_agent_info = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"sys_agent_info.xml")));
			pstmt.setString(5, user_agent_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_report_log");
			pstmt.setString(2, "历史报表");
			pstmt.setString(3, "历史报表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String report_log = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("sys_report_log.xml")));
			pstmt.setString(5, report_log);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_serial_info");
			pstmt.setString(2, "键值生成表");
			pstmt.setString(3, "键值生成表");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_serial_info = Streams.readAndClose(Streams
					.utf8r(getClass()
							.getResourceAsStream("sys_serial_info.xml")));
			pstmt.setString(5, sys_serial_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_event_info");
			pstmt.setString(2, "用户登陆历史记录表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_event_info = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"sys_event_info.xml")));
			pstmt.setString(5, sys_event_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_group_info");
			pstmt.setString(2, "用户组");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_group_info = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"sys_group_info.xml")));
			pstmt.setString(5, sys_group_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_user_group_info");
			pstmt.setString(2, "用户组");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_user_group_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_user_group_info.xml")));
			pstmt.setString(5, sys_user_group_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_chat_info");
			pstmt.setString(2, "聊天内容");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_chat_info = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"sys_chat_info.xml")));
			pstmt.setString(5, sys_chat_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_chat_detail_info");
			pstmt.setString(2, "聊天参与者");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_chat_detail_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_chat_detail_info.xml")));
			pstmt.setString(5, sys_chat_detail_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_notice_info");
			pstmt.setString(2, "通知");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_notice_info = Streams.readAndClose(Streams
					.utf8r(getClass()
							.getResourceAsStream("sys_notice_info.xml")));
			pstmt.setString(5, sys_notice_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_notice_detail_info");
			pstmt.setString(2, "通知参与者");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_notice_detail_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_notice_detail_info.xml")));
			pstmt.setString(5, sys_notice_detail_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_task_info");
			pstmt.setString(2, "待办事项");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_task_info = Streams
					.readAndClose(Streams.utf8r(getClass().getResourceAsStream(
							"sys_task_info.xml")));
			pstmt.setString(5, sys_task_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_task_detail_info");
			pstmt.setString(2, "待办事项参与者");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_task_detail_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_task_detail_info.xml")));
			pstmt.setString(5, sys_task_detail_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();

			pstmt = conn
					.prepareStatement("insert into granite_entities_info(entityId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "sys_org_role_info");
			pstmt.setString(2, "机构角色表");
			pstmt.setString(3, "");
			pstmt.setString(4, EntityType.TABLE.getName());
			String sys_org_role_info = Streams.readAndClose(Streams
					.utf8r(getClass().getResourceAsStream(
							"sys_org_role_info.xml")));
			pstmt.setString(5, sys_org_role_info);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			pstmt.close();
			
			log.debug("新建demo系统表成功");
		} catch (SQLException ex) {
			throw new EasyPlatformRuntimeException("PageTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}

	/**
	 * 建表
	 */
	public void test2() {
		log.debug("正在创建demo系统表");
		TableBean tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("sys_user_info.xml")));
		tb.setId("sys_user_info");
		createTable(tb);
		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("sys_role_info.xml")));
		tb.setId("sys_role_info");
		createTable(tb);
		tb = TransformerFactory.newInstance()
				.transformFromXml(
						TableBean.class,
						Streams.buff(getClass().getResourceAsStream(
								"sys_org_info.xml")));
		tb.setId("sys_org_info");
		createTable(tb);
		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("sys_menu_info.xml")));
		tb.setId("sys_menu_info");
		createTable(tb);
		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_user_role_info.xml")));
		tb.setId("sys_user_role_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_role_menu_info.xml")));
		tb.setId("sys_role_menu_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_user_org_info.xml")));
		tb.setId("sys_user_org_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_message_info.xml")));
		tb.setId("sys_message_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_agent_info.xml")));
		tb.setId("sys_agent_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_report_log.xml")));
		tb.setId("sys_report_log");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_serial_info.xml")));
		tb.setId("sys_serial_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_event_info.xml")));
		tb.setId("sys_event_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_group_info.xml")));
		tb.setId("sys_group_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_user_group_info.xml")));
		tb.setId("sys_user_group_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("sys_chat_info.xml")));
		tb.setId("sys_chat_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_chat_detail_info.xml")));
		tb.setId("sys_chat_detail_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_notice_info.xml")));
		tb.setId("sys_notice_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_notice_detail_info.xml")));
		tb.setId("sys_notice_detail_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass()
						.getResourceAsStream("sys_task_info.xml")));
		tb.setId("sys_task_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_task_detail_info.xml")));
		tb.setId("sys_task_detail_info");
		createTable(tb);

		tb = TransformerFactory.newInstance().transformFromXml(
				TableBean.class,
				Streams.buff(getClass().getResourceAsStream(
						"sys_org_role_info.xml")));
		tb.setId("sys_org_role_info");
		createTable(tb);
		
		log.debug("创建demo系统表成功");
		insert();
	}

	private void createTable(TableBean table) {
		Connection conn = null;
		Statement stmt = null;
		try {
			log.debug("createTable:" + table.getId());
			conn = JdbcTransactions.getConnection(ds2);
			stmt = conn.createStatement();
			String sql = "drop table if EXISTS " + table.getId();
			if (DaoUtils.getDbType(ds2.toString()) == DaoUtils.SYBASE)
				sql = "if exists(select 1 from sysobjects where name='"
						+ table.getId() + "') drop table "
						+ table.getId();
			stmt.execute(sql);
			DaoUtils.closeQuietly(stmt);
			stmt = conn.createStatement();
			stmt.execute(TableExpertFactory.createExpert(
					DaoUtils.getDbType(ds2.toString())).createSql(table));
		} catch (SQLException ex) {
			ex.printStackTrace();
			throw new EasyPlatformRuntimeException(table.getId(), ex);
		} finally {
			DaoUtils.closeQuietly(stmt);
		}
	}

	private void insert() {
		Reader reader = Streams.utf8r(getClass().getResourceAsStream(
				"db_sys.sql"));
		String data = Lang.readAll(reader);
		String[] sqls = data.split("\n");
		Connection conn = null;
		Statement stmt = null;
		log.debug("正在初始化系统表数据");
		String val = null;
		try {
			conn = JdbcTransactions.getConnection(ds2);
			for (String sql : sqls) {
				String s = sql.trim();
				if (!s.trim().startsWith("#") && !s.equals("")) {
					stmt = conn.createStatement();
					stmt.execute(s);
					log.debug("执行：{}", s);
					DaoUtils.closeQuietly(stmt);
					val = s;
				}
			}
			log.debug("初始化系统表数据成功");
		} catch (SQLException ex) {
			throw new ServiceException(val + ex.getMessage());
		} finally {
			DaoUtils.closeQuietly(stmt);
		}
	}
}
