/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.executor.jasper;

import cn.easyplatform.support.report.GroupCallback;
import net.sf.jasperreports.engine.JRAbstractScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JaserReportScriptlet extends JRAbstractScriptlet {

	private GroupCallback gcb;

	public JaserReportScriptlet(GroupCallback gcb) {
		this.gcb = gcb;
	}

	@Override
	public void beforeReportInit() throws JRScriptletException {
	}

	@Override
	public void afterReportInit() throws JRScriptletException {
	}

	@Override
	public void beforePageInit() throws JRScriptletException {
	}

	@Override
	public void afterPageInit() throws JRScriptletException {
	}

	@Override
	public void beforeColumnInit() throws JRScriptletException {
	}

	@Override
	public void afterColumnInit() throws JRScriptletException {

	}

	@Override
	public void beforeGroupInit(String groupName) throws JRScriptletException {
		gcb.onGroup(groupName);
	}

	@Override
	public void afterGroupInit(String groupName) throws JRScriptletException {
	}

	@Override
	public void beforeDetailEval() throws JRScriptletException {
	}

	@Override
	public void afterDetailEval() throws JRScriptletException {

	}

}
