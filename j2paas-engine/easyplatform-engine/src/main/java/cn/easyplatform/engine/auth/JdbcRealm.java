/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.auth;

import cn.easyplatform.contexts.Contexts;
import cn.easyplatform.dao.IdentityDao;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.interceptor.CommandContext;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JdbcRealm extends AuthorizingRealm {

    public JdbcRealm() {
    }

    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        try {
            CommandContext cc = Contexts.getCommandContext();
            IdentityDao dao = cc.getIdentityDao(true);
            UserDo user = dao.getUser(cc.getProjectService().getConfig()
                    .getAuthenticationQuery(), upToken.getUsername());
            if (cc.getEnv() != null)
                user.setDeviceType(cc.getEnv().getDeviceType());
            if (user == null)
                throw new UnknownAccountException("No account found for user ["
                        + upToken.getUsername() + "]");
            return new SimpleAuthenticationInfo(user,
                    user.getPassword(), "");
        } catch (Exception ex) {
            throw new AuthenticationException(ex);
        }
    }

    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {
        return null;
    }
}
