/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.auth;

import cn.easyplatform.shiro.PasswordEncoder;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Hash;

/**
 * Created by shiny on 2018-05-09.
 */
public class JDCredentialsMatcher extends HashedCredentialsMatcher implements PasswordEncoder {

    public JDCredentialsMatcher(){
        this.setHashAlgorithmName("md5");
        this.setHashIterations(3);
        this.setStoredCredentialsHexEncoded(true);
    }
    @Override
    public String encode(String data) {
        return hashProvidedCredentials(data,"epclouds",3).toHex();
    }

    @Override
    protected Hash hashProvidedCredentials(Object credentials, Object salt, int hashIterations) {
        return super.hashProvidedCredentials(credentials, "epclouds", 3);
    }
}
