/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.jasper;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.engine.runtime.jasper.ds.DatalistDataSource;
import cn.easyplatform.engine.runtime.jasper.ds.MemoryDataSource;
import cn.easyplatform.engine.runtime.jasper.ds.SimpleDataSource;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.ReportVo;
import cn.easyplatform.support.Closeable;
import cn.easyplatform.type.Constants;
import net.sf.jasperreports.engine.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
class CustomReportHandler extends AbstractReportHandler {

    private ReportVo rv;

    /**
     * @param cc
     * @param ctx
     * @param lists
     * @param rb
     */
    CustomReportHandler(CommandContext cc, RecordContext ctx,
                        Collection<ListContext> lists, JasperReportBean rb, ReportVo rv) {
        super(cc, ctx, lists, rb);
        this.rv = rv;
    }

    @Override
    protected JasperPrint doReport(JasperReport jr) throws JRException {
        Map<String, Object> parameters = createParameters(jr.getParameters());
        if (ctx != null)
            parameters.put("REPORT_DATA_SOURCE", new SimpleDataSource(ctx));
        List<Closeable> closeables = new ArrayList<Closeable>();
        if (lists != null) {
            for (ListContext lc : lists) {
                if (lc.getType().equals(Constants.DETAIL)
                        || lc.getType().equals(Constants.CATALOG)) {
                    Closeable c = new DatalistDataSource(cc, ctx, lc,
                            rb.getOnRecord(), rv.getListOrder());
                    parameters.put(lc.getId(), c);
                    closeables.add(c);
                }
            }
        }
        JRDataSource ds = null;
        if (!Strings.isBlank(rv.getInit())) {
            ds = new MemoryDataSource(cc, ctx, cc.getWorkflowContext()
                    .getReport(rv.getId()).getData(), rb.getOnRecord());
            closeables.add((Closeable) ds);
        }
        try {
            return ds == null ? JasperFillManager.fillReport(jr, parameters) : JasperFillManager.fillReport(jr, parameters, ds);
        } finally {
            for (Closeable c : closeables)
                c.close();
            closeables = null;
        }
    }

}
