/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.jasper;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
class QueryReportHandler extends AbstractReportHandler {

    /**
     * @param cc
     * @param ctx
     * @param lists
     * @param rb
     */
    QueryReportHandler(CommandContext cc, RecordContext ctx,
                       Collection<ListContext> lists, JasperReportBean rb) {
        super(cc, ctx, lists, rb);
    }

    @Override
    protected JasperPrint doReport(JasperReport jr) throws JRException {
        Map<String, Object> parameters = createParameters(jr.getParameters());
        try {
            if (Strings.isBlank(rb.getDsId())) {
                DataSource ds = cc.getProjectService().getDataSource();
                parameters.put("REPORT_CONNECTION",
                        JdbcTransactions.getConnection(ds));
            } else {
                DataSource ds = cc.getProjectService().getDataSource(
                        rb.getDsId());
                parameters.put("REPORT_CONNECTION",
                        JdbcTransactions.getConnection(ds));
            }
        } catch (SQLException ex) {
            throw new EasyPlatformWithLabelKeyException("report.connection.error",
                    ex, rb.getId());
        }
        return JasperFillManager.fillReport(jr, parameters);
    }

}
