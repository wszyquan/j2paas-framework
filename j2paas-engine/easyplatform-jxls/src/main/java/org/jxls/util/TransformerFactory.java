package org.jxls.util;

import java.io.InputStream;
import java.io.OutputStream;
import org.jxls.transform.Transformer;
import org.jxls.transform.poi.PoiTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Creates {@link Transformer} instances in runtime
 * @author Leonid Vysochyn
 *  @Fix 类替找
 */
public class TransformerFactory {

    private static Logger logger = LoggerFactory.getLogger(TransformerFactory.class);
    /**
     * @Fixed
     */
    public static Transformer createTransformer(InputStream inputStream, OutputStream outputStream) {
        try {
            Transformer transformer = PoiTransformer.createTransformer(inputStream, outputStream);
            return transformer;
        } catch (Exception e) {
            logger.error("Failed to create PoiTransformer", e);
            return null;
        }
    }

}
