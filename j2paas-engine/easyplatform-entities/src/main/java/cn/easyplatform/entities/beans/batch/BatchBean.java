/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.batch;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.EventLogic;

import javax.xml.bind.annotation.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = { "sourceDsId", "sourceSql", "targetTable", "processCode",
		"onBegin", "onRecord", "onEnd", "onException", "nextId",
		"transactionScope","batchUpdate" })
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "batch")
public class BatchBean extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8014818796618208345L;

	public final static int TRANSACTION_ALL = 1;

	public final static int TRANSACTION_RECORD = 2;

	public final static int TRANSACTION_CONTAINER = 3;

	@XmlElement
	private String sourceDsId;

	@XmlElement(required = true)
	private String sourceSql;

	@XmlElement
	private String targetTable;

	@XmlElement
	private String processCode;

	@XmlElement
	private EventLogic onBegin;

	@XmlElement
	private EventLogic onRecord;

	@XmlElement
	private EventLogic onEnd;

	@XmlElement
	private EventLogic onException;

	@XmlElement
	private String nextId;

	@XmlElement
	private  boolean batchUpdate;

	/**
	 * 事务范围 1:ALL 2:RECORD 3:CONTAINER
	 */
	@XmlElement
	private int transactionScope = TRANSACTION_ALL;

	public String getSourceDsId() {
		return sourceDsId;
	}

	public void setSourceDsId(String sourceDsId) {
		this.sourceDsId = sourceDsId;
	}

	public String getSourceSql() {
		return sourceSql;
	}

	public void setSourceSql(String sourceSql) {
		this.sourceSql = sourceSql;
	}

	public String getNextId() {
		return nextId;
	}

	public void setNextId(String nextId) {
		this.nextId = nextId;
	}

	public String getTargetTable() {
		return targetTable;
	}

	public void setTargetTable(String targetTable) {
		this.targetTable = targetTable;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public EventLogic getOnRecord() {
		return onRecord;
	}

	public void setOnRecord(EventLogic onRecord) {
		this.onRecord = onRecord;
	}

	public EventLogic getOnBegin() {
		return onBegin;
	}

	public void setOnBegin(EventLogic onBegin) {
		this.onBegin = onBegin;
	}

	public EventLogic getOnEnd() {
		return onEnd;
	}

	public void setOnEnd(EventLogic onEnd) {
		this.onEnd = onEnd;
	}

	/**
	 * @return the transactionScope
	 */
	public int getTransactionScope() {
		return transactionScope;
	}

	/**
	 * @param transactionScope
	 *            the transactionScope to set
	 */
	public void setTransactionScope(int transactionScope) {
		this.transactionScope = transactionScope;
	}

	/**
	 * @return the onException
	 */
	public EventLogic getOnException() {
		return onException;
	}

	/**
	 * @param onException the onException to set
	 */
	public void setOnException(EventLogic onException) {
		this.onException = onException;
	}

	public boolean isBatchUpdate() {
		return batchUpdate;
	}

	public void setBatchUpdate(boolean batchUpdate) {
		this.batchUpdate = batchUpdate;
	}
}
