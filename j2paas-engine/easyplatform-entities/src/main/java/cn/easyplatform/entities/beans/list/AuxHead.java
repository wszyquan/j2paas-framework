/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.list;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = { "style", "auxHeaders" })
@XmlAccessorType(XmlAccessType.NONE)
public class AuxHead implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7382638429039817300L;

	@XmlElement
	private String style;

	@XmlElement(name = "aux-header")
	private List<AuxHeader> auxHeaders;

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public List<AuxHeader> getAuxHeaders() {
		return auxHeaders;
	}

	public void setAuxHeaders(List<AuxHeader> auxHeaders) {
		this.auxHeaders = auxHeaders;
	}

}
