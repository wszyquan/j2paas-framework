/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.list;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.entities.helper.StringArrayAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"panel", "table", "condition", "orderBy", "groupBy",
        "query", "rowStyle", "headStyle", "footStyle", "onInit", "onBefore",
        "onAfter", "onRow", "onHeaderScript", "onRowScript", "onGroupScript",
        "onFooterScript", "auxHeads", "headers", "groups", "showType", "showTitle",
        "pageSize", "pageId", "editableColumns", "showPanel",
        "showRowNumbers", "groupName", "levelBy", "sizedByContent", "span",
        "checkmark", "multiple","matrixColumns","matrixRows"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "list")
public class ListBean extends BaseEntity implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -2663695376499191415L;

    /**
     * 查询面板 可由客户端决定是否显示
     */
    @XmlElement
    private String panel;

    /**
     * 除REPORT类型外，其余必输
     */
    @XmlElement
    private String table;

    /**
     * 条件语句
     */
    @XmlElement
    private String condition;

    /**
     * 排序语句
     */
    @XmlElement
    private String orderBy;

    /**
     * groupby
     */
    @XmlElement
    private String groupBy;

    /**
     * 初始逻辑
     */
    @XmlElement
    private EventLogic onInit;

    /**
     * 初始逻辑
     */
    @XmlElement
    private EventLogic onBefore;

    /**
     * 初始逻辑
     */
    @XmlElement
    private EventLogic onAfter;

    /**
     * 每笔计算
     */
    @XmlElement
    private EventLogic onRow;

    /**
     * 客户端事件逻辑：当列表的行数据生成后调用,在这里可以管理列的的控件
     */
    @XmlElement
    private String onRowScript;

    /**
     * 客户端事件逻辑：当列表头生成后调用
     */
    @XmlElement
    private String onHeaderScript;

    /**
     * 客户端事件逻辑：当列表有分组时调用
     */
    @XmlElement
    private String onGroupScript;

    /**
     * 客户端事件逻辑：当列表底生成调用
     */
    @XmlElement
    private String onFooterScript;

    /**
     * 自定义查询:支持多张表 不包括where 和 on 等条件从句，由condition来定义
     * 除REPORT，查询语句必须包含主键的栏位，例如表A的主键是name和level 可能的query值是select
     * a.x1,a.x2,a.name,a.level,a.xx,.... from A a left join B b
     */
    @XmlElement
    private String query;

    /**
     * 行风格
     */
    @XmlElement
    private String rowStyle;

    /**
     * 头部风格
     */
    @XmlElement
    private String headStyle;

    /**
     * 底部风格
     */
    @XmlElement
    private String footStyle;

    // ///////////////////////当列表直接挂在功能上以下属性起作用/////////////////////////////
    @XmlElement
    private String showType;
    @XmlElement
    private int pageSize;
    @XmlElement
    private String pageId;
    @XmlElement
    private String editableColumns;
    @XmlElement
    private String groupName;
    @XmlElement
    private String levelBy;
    @XmlElement
    private boolean showTitle = true;
    @XmlElement
    private boolean showPanel = true;
    @XmlElement
    private boolean showRowNumbers = true;
    @XmlElement
    private boolean sizedByContent;
    @XmlElement
    private boolean span;
    @XmlElement
    private boolean checkmark;
    @XmlElement
    private boolean multiple;
    // /////////////////////////END////////////////////////////

    @XmlElement(name = "aux-head")
    private List<AuxHead> auxHeads;

    @XmlElement(name = "header")
    private List<Header> headers;

    @XmlElement(name = "group")
    private List<Group> groups;

    //维度列(column)
    @XmlElement(name = "matrixColumns")
    @XmlJavaTypeAdapter(value = StringArrayAdapter.class)
    private String[] matrixColumns;

    //布局列(row)
    @XmlElement(name = "matrixRows")
    @XmlJavaTypeAdapter(value = StringArrayAdapter.class)
    private String[] matrixRows;

    public String getPanel() {
        return panel;
    }

    public void setPanel(String panel) {
        this.panel = panel;
    }

    public List<AuxHead> getAuxHeads() {
        return auxHeads;
    }

    public void setAuxHeads(List<AuxHead> auxHeads) {
        this.auxHeads = auxHeads;
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public void setHeaders(List<Header> headers) {
        this.headers = headers;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getOnRowScript() {
        return onRowScript;
    }

    public void setOnRowScript(String onRowScript) {
        this.onRowScript = onRowScript;
    }

    public String getOnHeaderScript() {
        return onHeaderScript;
    }

    public void setOnHeaderScript(String onHeaderScript) {
        this.onHeaderScript = onHeaderScript;
    }

    public String getOnGroupScript() {
        return onGroupScript;
    }

    public void setOnGroupScript(String onGroupScript) {
        this.onGroupScript = onGroupScript;
    }

    public String getOnFooterScript() {
        return onFooterScript;
    }

    public void setOnFooterScript(String onFooterScript) {
        this.onFooterScript = onFooterScript;
    }

    /**
     * @return the groupBy
     */
    public String getGroupBy() {
        return groupBy;
    }

    /**
     * @param groupBy the groupBy to set
     */
    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    /**
     * @return
     */
    public String getRowStyle() {
        return rowStyle;
    }

    /**
     * @param rowStyle
     */
    public void setRowStyle(String rowStyle) {
        this.rowStyle = rowStyle;
    }

    public String getHeadStyle() {
        return headStyle;
    }

    public void setHeadStyle(String headStyle) {
        this.headStyle = headStyle;
    }

    public String getFootStyle() {
        return footStyle;
    }

    public void setFootStyle(String footStyle) {
        this.footStyle = footStyle;
    }

    public EventLogic getOnInit() {
        return onInit;
    }

    public void setOnInit(EventLogic onInit) {
        this.onInit = onInit;
    }

    public EventLogic getOnRow() {
        return onRow;
    }

    public void setOnRow(EventLogic onRow) {
        this.onRow = onRow;
    }

    public EventLogic getOnBefore() {
        return onBefore;
    }

    public void setOnBefore(EventLogic onBefore) {
        this.onBefore = onBefore;
    }

    public EventLogic getOnAfter() {
        return onAfter;
    }

    public void setOnAfter(EventLogic onAfter) {
        this.onAfter = onAfter;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public boolean isShowTitle() {
        return showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getEditableColumns() {
        return editableColumns;
    }

    public void setEditableColumns(String editableColumns) {
        this.editableColumns = editableColumns;
    }

    public boolean isShowPanel() {
        return showPanel;
    }

    public void setShowPanel(boolean showPanel) {
        this.showPanel = showPanel;
    }

    public boolean isShowRowNumbers() {
        return showRowNumbers;
    }

    public void setShowRowNumbers(boolean showRowNumbers) {
        this.showRowNumbers = showRowNumbers;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getLevelBy() {
        return levelBy;
    }

    public void setLevelBy(String levelBy) {
        this.levelBy = levelBy;
    }

    public boolean isSizedByContent() {
        return sizedByContent;
    }

    public void setSizedByContent(boolean sizedByContent) {
        this.sizedByContent = sizedByContent;
    }

    public boolean isSpan() {
        return span;
    }

    public void setSpan(boolean span) {
        this.span = span;
    }

    public boolean isCheckmark() {
        return checkmark;
    }

    public void setCheckmark(boolean checkmark) {
        this.checkmark = checkmark;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public String[] getMatrixColumns() {
        return matrixColumns;
    }

    public void setMatrixColumns(String[] matrixColumns) {
        this.matrixColumns = matrixColumns;
    }

    public String[] getMatrixRows() {
        return matrixRows;
    }

    public void setMatrixRows(String[] matrixRows) {
        this.matrixRows = matrixRows;
    }

    @Override
    public ListBean clone() {
        try {
            return (ListBean) super.clone();
        } catch (Exception ex) {
        }
        return null;
    }
}
