/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.swift;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TagVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;//

	private String name;// link name

	private String tagName;// tag name

	private Object[] keys;

	private Object value;

	/**
	 * @param id
	 * @param name
	 * @param tagName
	 * @param keys
	 */
	public TagVo(String id, String name, String tagName, Object[] keys,
			Object value) {
		this.id = id;
		this.name = name;
		this.tagName = tagName;
		this.keys = keys;
		this.value = value;
	}

	/**
	 * @param id
	 * @param name
	 * @param tagName
	 * @param keys
	 */
	public TagVo(String id, String name, String tagName, Object[] keys) {
		this(id, name, tagName, keys, null);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the tagName
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * @return the keys
	 */
	public Object[] getKeys() {
		return keys;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

}
