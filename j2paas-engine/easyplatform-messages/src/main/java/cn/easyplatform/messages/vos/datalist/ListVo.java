/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import cn.easyplatform.type.ListRowVo;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	// 查询面板
	private String panel;

	// 总笔数
	private int totalSize;

	// 查询数据
	private List<ListRowVo> data;

	// 分组信息
	private List<ListGroupVo> groups;

	// 多表头
	private List<ListAuxHeadVo> auxHeads;

	// 实际表头
	private List<ListHeaderVo> headers;

	// 自定义查询
	private boolean isCustom;

	// 错误信息
	private String errMsg;

	// onRow事件
	private String onRow;

	// 生成表头事件
	private String onHeader;

	// 生成分组事件
	private String onGroup;

	// 分成表底事件
	private String onFooter;

	// 行风格
	private String rowStyle;
	
	// 头风格
	private String headStyle;
	
	//多重表头风格
	private String auxHeadStyle;
	
	// 底风格
	private String footStyle;

	//维度列
	private String[] matrixColumns;

	//布局列
	private String[] matrixRows;

	public String[] getMatrixColumns() {
		return matrixColumns;
	}

	public void setMatrixColumns(String[] matrixColumns) {
		this.matrixColumns = matrixColumns;
	}

	public String[] getMatrixRows() {
		return matrixRows;
	}

	public void setMatrixRows(String[] matrixRows) {
		this.matrixRows = matrixRows;
	}

	public ListVo(String name) {
		this.name = name;
	}

	public List<ListRowVo> getData() {
		return data;
	}

	public void setData(List<ListRowVo> data) {
		this.data = data;
	}

	public List<ListAuxHeadVo> getAuxHeads() {
		return auxHeads;
	}

	public void setAuxHeads(List<ListAuxHeadVo> auxHeads) {
		this.auxHeads = auxHeads;
	}

	public List<ListHeaderVo> getHeaders() {
		return headers;
	}

	public void setHeaders(List<ListHeaderVo> headers) {
		this.headers = headers;
	}

	public String getPanel() {
		return panel;
	}

	public void setPanel(String panel) {
		this.panel = panel;
	}

	public List<ListGroupVo> getGroups() {
		return groups;
	}

	public void setGroups(List<ListGroupVo> groups) {
		this.groups = groups;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	public String getName() {
		return name;
	}

	public boolean isCustom() {
		return isCustom;
	}

	public void setCustom(boolean isCustom) {
		this.isCustom = isCustom;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getOnRow() {
		return onRow;
	}

	public void setOnRow(String onRow) {
		this.onRow = onRow;
	}

	/**
	 * @return the onHeader
	 */
	public String getOnHeader() {
		return onHeader;
	}

	/**
	 * @param onHeader
	 *            the onHeader to set
	 */
	public void setOnHeader(String onHeader) {
		this.onHeader = onHeader;
	}

	/**
	 * @return the onGroup
	 */
	public String getOnGroup() {
		return onGroup;
	}

	/**
	 * @param onGroup
	 *            the onGroup to set
	 */
	public void setOnGroup(String onGroup) {
		this.onGroup = onGroup;
	}

	/**
	 * @return the onFooter
	 */
	public String getOnFooter() {
		return onFooter;
	}

	/**
	 * @param onFooter
	 *            the onFooter to set
	 */
	public void setOnFooter(String onFooter) {
		this.onFooter = onFooter;
	}

	public String getRowStyle() {
		return rowStyle;
	}

	public void setRowStyle(String rowStyle) {
		this.rowStyle = rowStyle;
	}

	public String getHeadStyle() {
		return headStyle;
	}

	public void setHeadStyle(String headStyle) {
		this.headStyle = headStyle;
	}

	public String getFootStyle() {
		return footStyle;
	}

	public void setFootStyle(String footStyle) {
		this.footStyle = footStyle;
	}

	public String getAuxHeadStyle() {
		return auxHeadStyle;
	}

	public void setAuxHeadStyle(String auxHeadStyle) {
		this.auxHeadStyle = auxHeadStyle;
	}

}
