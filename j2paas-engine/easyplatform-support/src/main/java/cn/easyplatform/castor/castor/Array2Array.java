/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import cn.easyplatform.castor.Castor;
import cn.easyplatform.castor.FailToCastObjectException;
import cn.easyplatform.lang.Lang;

import java.lang.reflect.Array;

public class Array2Array extends Castor<Object, Object> {

	public Array2Array() {
		this.fromClass = Array.class;
		this.toClass = Array.class;
	}

	@Override
	public Object cast(Object src, Class<?> toType, String... args)
			throws FailToCastObjectException {
		return Lang.array2array(src, toType.getComponentType());
	}

}
