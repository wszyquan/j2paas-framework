/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.interceptor;

import cn.easyplatform.cfg.EngineConfiguration;
import cn.easyplatform.contexts.Contexts;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CommandContextInterceptor extends AbstractCommandInterceptor {

    private static Logger log = LoggerFactory.getLogger(CommandContextInterceptor.class);

    protected CommandContextFactory commandContextFactory;

    protected EngineConfiguration engineConfiguration;

    /**
     * @param commandContextFactory
     * @param engineConfiguration
     */
    public CommandContextInterceptor(
            CommandContextFactory commandContextFactory,
            EngineConfiguration engineConfiguration) {
        this.commandContextFactory = commandContextFactory;
        this.engineConfiguration = engineConfiguration;
    }

    /*
     *
     * 设置当前执行动作的上下文环境
     */
    @Override
    public IResponseMessage<?> execute(Command command) {
        try {
            CommandContext cc = commandContextFactory.createCommandContext(
                    engineConfiguration, command);
            Contexts.set(CommandContext.class, cc);
            IResponseMessage<?> resp = next.execute(command);
            cc.touch();
            return resp;
        } catch (Throwable ex) {
            return MessageUtils.getMessage(ex, log);
        } finally {
            Contexts.clear();
        }
    }

}
