/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.scripting.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MappingScriptParser extends AbstractScriptParser {

	private List<String> mappingFields = new ArrayList<String>();

	@Override
	protected void setMappingField(String field) {
		int sep = field.indexOf(".");
		if (sep > 0)
			field = field.substring(0, sep);
		mappingFields.add(field);
	}

	@Override
	public List<String> getMappingFields() {
		return mappingFields;
	}

}
